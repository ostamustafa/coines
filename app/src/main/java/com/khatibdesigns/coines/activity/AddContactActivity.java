package com.khatibdesigns.coines.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.util.adapter.ContactsAdapter;
import com.khatibdesigns.coines.util.database.AppDatabase;
import com.khatibdesigns.coines.util.model.User;

import java.util.ArrayList;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AddContactActivity extends AppCompatActivity {

    private RecyclerView searchList;
    private User me;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        ActionBar bar = getSupportActionBar();
        if (bar != null)
            bar.setDisplayHomeAsUpEnabled(true);

        me = AppDatabase.getDatabase(this).userDao().getUser(FirebaseAuth.getInstance().getCurrentUser().getUid());
        EditText search = findViewById(R.id.search);
        searchList = findViewById(R.id.search_list);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equals(me.getUsername()))
                    FirebaseFirestore.getInstance().collection("users")
                            .whereEqualTo("username", s.toString())
                            .get()
                            .addOnSuccessListener(success -> {
                                ArrayList<User> list = new ArrayList<>();
                                for (DocumentSnapshot snapshot : success.getDocuments()) {
                                    User user = new User(
                                            snapshot.getId(),
                                            snapshot.getString("display_name"),
                                            snapshot.getString("username"),
                                            snapshot.getString("avatar_url"),
                                            snapshot.getString("phone_number"),
                                            snapshot.getBoolean("online_status"),
                                            snapshot.getString("push_token"),
                                            true);
                                    list.add(user);
                                }
                                ContactsAdapter adapter = new ContactsAdapter(AddContactActivity.this, list, (v, position) -> {
                                    User user = list.get(position);
                                    startActivity(new Intent(AddContactActivity.this, ContactDetailsActivity.class)
                                    .putExtra(ContactDetailsActivity.USER, user));
                                });
                                LinearLayoutManager manager = new LinearLayoutManager(AddContactActivity.this);
                                manager.setOrientation(RecyclerView.VERTICAL);
                                searchList.setAdapter(adapter);
                                searchList.setLayoutManager(manager);
                            })
                            .addOnFailureListener(failure -> Log.d("Query", failure.getMessage()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
