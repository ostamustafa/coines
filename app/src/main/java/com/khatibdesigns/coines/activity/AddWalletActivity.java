package com.khatibdesigns.coines.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.util.model.Currency;
import com.khatibdesigns.coines.util.sprint.APIEndPoint;
import com.khatibdesigns.coines.util.sprint.Method;
import com.khatibdesigns.coines.util.sprint.Sprint;
import com.khatibdesigns.coines.util.sprint.SprintListener;
import com.khatibdesigns.coines.util.sprint.header.Accept;
import com.khatibdesigns.coines.util.sprint.header.ContentType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class AddWalletActivity extends AppCompatActivity {
    private Spinner currencySpinner;
    private ArrayList<Currency> currencyList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_wallet);
        currencySpinner = findViewById(R.id.currency_spinner);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        HashMap<String, String> args = new HashMap<>();
        args.put("api_key", "kun9Eoptepjy2eQ09uRB7foQpOKnnC");
        Sprint.instance(Method.POST)
                .headers(ContentType.JSON, Accept.JSON)
                .request(APIEndPoint.CURRENCIES, args)
                .execute(new SprintListener() {
                    @Override
                    public void onSuccess(JSONObject data) {
                        try {
                            currencyList = new ArrayList<>();
                            JSONArray currencies = data.getJSONArray("currencies");

                            for (int i = 0; i < currencies.length(); i++) {
                                JSONObject jsonCurrency = currencies.getJSONObject(i);
                                Currency currency = new Currency(
                                        jsonCurrency.optString("ticker"),
                                        jsonCurrency.optString("name"),
                                        jsonCurrency.optString("position"),
                                        jsonCurrency.optBoolean("def_left"),
                                        jsonCurrency.optBoolean("def_right"),
                                        jsonCurrency.optDouble("network_fee"),
                                        jsonCurrency.optDouble("coines_fee"),
                                        jsonCurrency.optDouble("min_for_exchange"),
                                        jsonCurrency.optString("currency_logo")
                                );
                                currencyList.add(currency);
                            }
                            currencyList.add(new Currency("Currencies"));
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddWalletActivity.this, android.R.layout.simple_list_item_1) {

                                @Override
                                public @NonNull
                                View getView(int position, View convertView, @NonNull ViewGroup parent) {
                                    View v = super.getView(position, convertView, parent);
                                    if (position == getCount()) {
                                        TextView text = v.findViewById(android.R.id.text1);
                                        text.setText("");
                                        text.setHint((getItem(getCount())));
                                    } else {
                                        TextView text = v.findViewById(android.R.id.text1);
                                        text.setText(getItem(getCount()));
                                    }

                                    return v;
                                }

                                @Override
                                public int getCount() {
                                    // you don't display last item. It is used as hint.
                                    return super.getCount() - 1;
                                }

                            };

                            for (Currency currency : currencyList)
                                adapter.add(currency.getName());

                            currencySpinner.setAdapter(adapter);
                            currencySpinner.setSelection(adapter.getCount());
                            currencySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    if (position != parent.getCount()) {
                                        TextView text = view.findViewById(android.R.id.text1);
                                        text.setText(currencyList.get(position).getName());
                                        currencySpinner.setPadding(0,5,0,5);

                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(JSONObject data, int statusCode) {

                    }
                });
        TextView txtAddress = findViewById(R.id.txt_address);
        String address = (String) txtAddress.getText();
        Button saveWallet = findViewById(R.id.btn_save_wallet);
        saveWallet.setOnClickListener(v -> {
            HashMap<String, String> args1 = new HashMap<>();
            args1.put("api_key", "kun9Eoptepjy2eQ09uRB7foQpOKnnC");
            args1.put("address", address);
            Sprint.instance(Method.POST)
                    .headers(ContentType.JSON, Accept.JSON)
                    .request(APIEndPoint.VALIDATE_ADDRESS, args1)
                    .execute(new SprintListener() {
                        @Override
                        public void onSuccess(JSONObject data) {
                            String valid = data.optString("address");
                            if( valid.equals("valid")){

                            }
                        }

                        @Override
                        public void onFailure(JSONObject data, int statusCode) {

                        }
                    });
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem notification = menu.findItem(R.id.action_notifications);
        notification.getActionView().setOnClickListener(view -> startActivity(new Intent(this, NotificationsActivity.class)));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
