package com.khatibdesigns.coines.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.util.database.AppDatabase;
import com.khatibdesigns.coines.util.model.User;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import de.hdodenhof.circleimageview.CircleImageView;

public class ContactDetailsActivity extends AppCompatActivity {

    public static final String USER = ContactDetailsActivity.class.getName() + ".USER";
    public static final String DIALOG = ContactDetailsActivity.class.getName() + ".DIALOG";

    private User user;
    private TextView contact;
    private LinearLayout contactWrapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details);
        ActionBar bar = getSupportActionBar();
        if (bar != null)
            bar.setDisplayHomeAsUpEnabled(true);

        user = (User) getIntent().getSerializableExtra(USER);
        TextView displayName = findViewById(R.id.display_name);
        displayName.setText(user.getDisplayName());
        TextView status = findViewById(R.id.status);
        if (user.isOnline()) {
            status.setText(R.string.online);
            status.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.online, 0, 0, 0);
        } else {
            status.setText(R.string.offline);
            status.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.offline, 0, 0, 0);
        }
        status.setCompoundDrawablePadding(10);
        TextView phoneNumber = findViewById(R.id.phone_number);
        phoneNumber.setText(user.getPhoneNumber());
        CircleImageView avatar = findViewById(R.id.avatar);
        if (user.getAvatarUrl().isEmpty())
            Picasso.get().load(R.drawable.placeholder).into(avatar);
        else
            Picasso.get().load(user.getAvatarUrl()).error(R.drawable.placeholder).into(avatar);
        contact = findViewById(R.id.contact);
        contactWrapper = findViewById(R.id.contact_wrapper);
        if (AppDatabase.getDatabase(this).userDao().exists(user.getId())) {
            removeContact();
        } else {
            addContact();
        }
        TextView username = findViewById(R.id.username);
        username.setText(user.getUsername());
    }

    private void addContact() {
        contact.setText(R.string.action_add_contact);
        contactWrapper.setOnClickListener(view -> {
            AppDatabase.getDatabase(this).userDao().insert(user);
            removeContact();
        });
    }

    private void removeContact() {
        contact.setText(R.string.remove_contact);
        contactWrapper.setOnClickListener(view -> {
            AppDatabase.getDatabase(this).userDao().delete(user);
            addContact();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem notification = menu.findItem(R.id.action_notifications);
        notification.getActionView().setOnClickListener(view -> startActivity(new Intent(this, NotificationsActivity.class)));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
