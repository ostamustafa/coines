package com.khatibdesigns.coines.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.util.model.Exchange;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

public class ExchangeStatusActivity extends AppCompatActivity {

    public static final String EXCHANGE = ExchangeStatusActivity.class.getName() + ".EXCHANGE";

    private TextView statusInfo, statusMessage, exchangeMessage, txtStatus;
    private ImageView statusIcon;
    private LinearLayout statusWaiting, statusSuccessFail, exchangeAction, otherSide, provideLink, btnExchange;
    private Exchange exchange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange_status);

        ActionBar bar = getSupportActionBar();
        statusInfo = findViewById(R.id.status_info);
        statusMessage = findViewById(R.id.status_message);
        statusIcon = findViewById(R.id.status_icon);
        statusWaiting = findViewById(R.id.status_waiting);
        statusSuccessFail = findViewById(R.id.status_success_fail);
        exchangeAction = findViewById(R.id.exchange_action);
        exchangeMessage = findViewById(R.id.exchange_message);
        otherSide = findViewById(R.id.other_side);
        txtStatus = findViewById(R.id.txt_status);
        provideLink = findViewById(R.id.provide_link);
        btnExchange = findViewById(R.id.btn_exchange);

        if (bar != null)
            bar.setDisplayHomeAsUpEnabled(true);

        if (getIntent().getSerializableExtra(EXCHANGE) != null)
            exchange = (Exchange) getIntent().getSerializableExtra(EXCHANGE);

        if (exchange.getTime() != null) {
            statusInfo.setVisibility(View.GONE);
            statusMessage.setText(R.string.status_waiting);
            statusMessage.setTextColor(Color.parseColor("#15A6E6"));
            statusIcon.setImageResource(R.drawable.sandwatch);
            statusWaiting.setVisibility(View.VISIBLE);
            statusSuccessFail.setVisibility(View.GONE);
        } else if (exchange.getStatus() != null) {
            statusInfo.setVisibility(View.VISIBLE);
            statusWaiting.setVisibility(View.GONE);
            statusSuccessFail.setVisibility(View.VISIBLE);
            exchangeAction.setVisibility(View.VISIBLE);
            if (exchange.getStatus().equals(getString(R.string.failed))) {
                exchangeMessage.setText(R.string.restart_exchange);
                exchangeMessage.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_restart, 0, 0, 0);
                exchangeMessage.setCompoundDrawablePadding(5);
                statusInfo.setText(R.string.status_info_fail);
                statusMessage.setText(R.string.status_failed);
                statusMessage.setTextColor(Color.parseColor("#CC0000"));
                statusIcon.setImageResource(R.drawable.failed);
                txtStatus.setText(R.string.failure);
                otherSide.setVisibility(View.VISIBLE);
                provideLink.setVisibility(View.GONE);
            } else {
                exchangeMessage.setText(R.string.cancel_exchange);
                exchangeMessage.setCompoundDrawablesWithIntrinsicBounds(R.drawable.disabled, 0, 0, 0);
                exchangeMessage.setCompoundDrawablePadding(5);
                provideLink.setVisibility(View.VISIBLE);
                otherSide.setVisibility(View.GONE);
                statusMessage.setTextColor(Color.parseColor("#3CB95D"));
                if (exchange.getStatus().equals(getString(R.string.tap_details))) {
                    statusMessage.setText(R.string.status_received);
                    statusInfo.setText(R.string.status_info_waiting);
                } else {
                    otherSide.setVisibility(View.VISIBLE);
                    provideLink.setVisibility(View.GONE);
                    exchangeAction.setVisibility(View.GONE);
                    statusMessage.setText(R.string.status_both_success);
                    statusInfo.setText(R.string.status_info_success);
                }
                statusIcon.setImageResource(R.drawable.successful);
                txtStatus.setText(R.string.successful);
            }
        }

        btnExchange.setOnClickListener(view -> {
            LinearLayout layout = (LinearLayout) view;
            TextView text = layout.findViewById(R.id.exchange_message);
            if (text.getText().toString().equals(getString(R.string.cancel_exchange))) {
                MaterialDialog.Builder builder = new MaterialDialog.Builder(this);
                builder.title(R.string.cancel_exchange)
                        .content("Are you sure you want to cancel this?")
                        .cancelable(true)
                        .positiveText("Cancel")
                        .positiveColor(Color.parseColor("#DB0000"))
                        .negativeText("Back")
                        .build()
                        .show();
            }
        });
//        RotateAnimation anim = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//        anim.setInterpolator(new LinearInterpolator());
//        anim.setRepeatCount(Animation.INFINITE);
//        anim.setDuration(2500);
//
//        statusIcon.setAnimation(anim);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}