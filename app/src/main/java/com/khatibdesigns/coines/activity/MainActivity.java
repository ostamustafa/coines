package com.khatibdesigns.coines.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.util.adapter.MainPagerAdapter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private MainPagerAdapter mainPagerAdapter;
    private ViewPager mViewPager;
    private MenuItem notification;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        mainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mainPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        init();
    }

    private void init() {
        TextView notice = navigationView.findViewById(R.id.notice);
        String strNotice = "No service fee when you selling or buying SMS until 10/06/2018. ";
        String learnMore = "Learn more »";
        SpannableStringBuilder span = new SpannableStringBuilder();
        span.append(strNotice);
        int start = span.length();
        span.append(learnMore);
        span.setSpan(new ForegroundColorSpan(Color.parseColor("#15A6E6")), start, span.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        notice.setText(span);

        TextView btcPrice = navigationView.findViewById(R.id.btc_price);
        String strBtcPrice = "BTC Price:";
        String price = " 8957.35 $";
        SpannableStringBuilder spanPrice = new SpannableStringBuilder();
        start = spanPrice.length();
        spanPrice.append(strBtcPrice);
        spanPrice.setSpan(new StyleSpan(Typeface.BOLD), start, spanPrice.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spanPrice.setSpan(new ForegroundColorSpan(Color.parseColor("#333333")), start, spanPrice.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spanPrice.append(price);
        btcPrice.setText(spanPrice);
    }


    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        notification = menu.findItem(R.id.action_notifications);
        notification.getActionView().setOnClickListener(view -> startActivity(new Intent(this, NotificationsActivity.class)));
        TextView badge = notification.getActionView().findViewById(R.id.notification_badge);
        badge.setVisibility(View.GONE);
        new Handler().postDelayed(() -> {
            badge.setVisibility(View.VISIBLE);
            badge.setText("2");
        }, 4000);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_create_new_exchange:
                startActivity(new Intent(this, NewStep1Activity.class));
                break;
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            case R.id.action_messaging:
                startActivity(new Intent(this, ChatActivity.class));
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
