package com.khatibdesigns.coines.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.util.database.AppDatabase;
import com.khatibdesigns.coines.util.holder.IncomingMessageViewHolder;
import com.khatibdesigns.coines.util.model.Dialog;
import com.khatibdesigns.coines.util.model.Message;
import com.khatibdesigns.coines.util.model.Request;
import com.khatibdesigns.coines.util.model.User;
import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.stfalcon.chatkit.utils.DateFormatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import de.hdodenhof.circleimageview.CircleImageView;

public class MessagesActivity extends AppCompatActivity implements DateFormatter.Formatter {

    private MessageInput input;
    private AppDatabase db;
    private Dialog dialog;
    private CircleImageView userPic;
    private TextView username;
    private MessagesList messagesList;
    private MessagesListAdapter<Message> adapter;
    private User me, user;
    private FirebaseFirestore firestore;

    public static final String DIALOG = MessagesActivity.class.getName() + ".DIALOG";
    public static final String USER = MessagesActivity.class.getName() + ".USER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        init();
        if (user != null) {
            initUser();
        } else {
            if (dialog.getAdminId() != null)
                initDialog();
            else {
                user = dialog.getUsers().get(0).getId().equals(me.getId()) ? dialog.getUsers().get(1) : dialog.getUsers().get(0);
                initUser();
            }
            populate();
        }

        input.getInputEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int visibility = s.length() == 0 ? View.VISIBLE : View.GONE;
                input.findViewById(R.id.attachmentButtonSpace).setVisibility(visibility);
                input.findViewById(R.id.attachmentButton).setVisibility(visibility);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        input.getButton().setOnClickListener(v -> {
            if (!input.getInputEditText().getText().toString().trim().isEmpty()) {
                if (dialog == null) {
                    input.getButton().setEnabled(false);
                    HashMap<String, Object> requestMap = new HashMap<>();
                    ArrayList<DocumentReference> members = new ArrayList<>();
                    DocumentReference refMe = firestore.collection("users").document(me.getId());
                    DocumentReference refUser = firestore.collection("users").document(user.getId());
                    members.add(refMe);
                    members.add(refUser);
                    requestMap.put("members", members);
                    requestMap.put("sender_id", refMe);
                    requestMap.put("preview_message", input.getInputEditText().getText().toString());
                    requestMap.put("created_at", new Timestamp(new Date()));
                    firestore.collection("requests").add(requestMap)
                            .addOnSuccessListener(success -> {
                                Request request = new Request(success.getId(), requestMap.get("preview_message").toString(), refUser.getId(), ((Timestamp) requestMap.get("created_at")).toDate(), true, true);
                                db.requestDao().insert(request);
                                finish();
                            });
                } else {
                    String msg = input.getInputEditText().getText().toString();
                    DocumentReference refMe = firestore.collection("users").document(me.getId());
                    DocumentReference refDialog = firestore.collection("dialogs").document(dialog.getId());
                    Timestamp createdAt = new Timestamp(new Date());
                    HashMap<String, Object> data = new HashMap<>();
                    data.put("message", msg);
                    data.put("created_at", createdAt);
                    data.put("sender_id", refMe);
                    data.put("dialog_id", refDialog);
                    data.put("media_url", "");
                    DocumentReference refMessage = firestore.collection("messages").document();
                    refMessage.set(data);
                    Message message = new Message(refMessage.getId(), msg, me, dialog.getId(), createdAt.toDate());
                    db.messageDao().insert(message);
                    input.getInputEditText().setText("");
                }
            }
        });
    }

    private void initUser() {
        MessageHolders holders = new MessageHolders();
        holders.setIncomingTextLayout(R.layout.layout_incoming_message);
        holders.setOutcomingTextLayout(R.layout.layout_outcoming_message);

        adapter = new MessagesListAdapter<>(me.getId(), holders, (imageView, url, payload) ->
                Picasso.get().load(url).into(imageView)
        );
        adapter.setDateHeadersFormatter(this);
        messagesList.setAdapter(adapter);
    }

    private void initDialog() {
        MessageHolders holders = new MessageHolders();
        holders.setIncomingTextConfig(IncomingMessageViewHolder.class, R.layout.layout_incoming_message);
        holders.setOutcomingTextLayout(R.layout.layout_outcoming_message);

        adapter = new MessagesListAdapter<>(me.getId(), holders, (imageView, url, payload) ->
                Picasso.get().load(url).into(imageView)
        );
        adapter.setDateHeadersFormatter(this);
        messagesList.setAdapter(adapter);
    }

    @Override
    public String format(Date date) {
        if (DateFormatter.isToday(date))
            return "Today";
        else if (DateFormatter.isYesterday(date))
            return "Yesterday";
        else if (sameWeek(date))
            return DateFormatter.format(date, "EEEE");
        else if (DateFormatter.isCurrentYear(date))
            return DateFormatter.format(date, "dd MMM");
        return DateFormatter.format(date, "dd MMM yyyy");
    }

    private boolean sameWeek(Date date) {
        Calendar now = Calendar.getInstance();
        Calendar current = Calendar.getInstance();
        current.setTime(date);
        return now.get(Calendar.WEEK_OF_YEAR) == current.get(Calendar.WEEK_OF_YEAR);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_messages, menu);
        MenuItem notification = menu.findItem(R.id.action_notifications);
        notification.getActionView().setOnClickListener(view -> startActivity(new Intent(this, NotificationsActivity.class)));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            case R.id.action_clear_history:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void populate() {
        ArrayList<Message> messages = (ArrayList<Message>) db.messageDao().getMessagesForDialog(dialog.getId());
        for (Message message : messages)
            message.setUser(db.userDao().getUser(message.getUserId()));
        adapter.addToEnd(messages, false);
        DocumentReference refDialog = firestore.collection("dialogs").document(dialog.getId());
        firestore.collection("messages")
                .whereEqualTo("dialog_id", refDialog)
                .orderBy("created_at", Query.Direction.DESCENDING)
                .addSnapshotListener(this, (snapshots, e) -> {
                    if (e != null)
                        e.printStackTrace();
                    if (snapshots != null) {
                        for (DocumentChange change : snapshots.getDocumentChanges()) {
                            if (change.getType().equals(DocumentChange.Type.ADDED)) {
                                QueryDocumentSnapshot snapshot = change.getDocument();
                                String msg = snapshot.getString("message");
                                String dialogId = dialog.getId();
                                Date createdAt = snapshot.getDate("created_at");
                                String senderId = snapshot.getDocumentReference("sender_id").getId();
                                User sender = senderId.contentEquals(me.getId()) ? me : user;
                                Message message = new Message(snapshot.getId(), msg, sender, dialogId, createdAt);
                                db.messageDao().insert(message);
                                if (!adapter.update(message))
                                    adapter.addToStart(message, true);
                            }
                        }
                    }
                });
    }

    private void init() {
        if (getIntent().hasExtra(DIALOG))
            dialog = (Dialog) getIntent().getSerializableExtra(DIALOG);
        else
            user = (User) getIntent().getSerializableExtra(USER);
        userPic = findViewById(R.id.user_pic);
        username = findViewById(R.id.username);
        messagesList = findViewById(R.id.messagesList);
        input = findViewById(R.id.input);
        db = AppDatabase.getDatabase(this);
        firestore = FirebaseFirestore.getInstance();
        me = db.userDao().getUser(FirebaseAuth.getInstance().getCurrentUser().getUid());
        initToolbar();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setContentInsetStartWithNavigation(0);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LinearLayout toolbarLayout = findViewById(R.id.toolbar_layout);

        if (dialog != null) {
            username.setText(dialog.getDialogName());
            loadPic(dialog.getDialogPhoto());
            toolbarLayout.setOnClickListener(v ->
                    startActivity(new Intent(this, ContactDetailsActivity.class).putExtra(ContactDetailsActivity.DIALOG, dialog))
            );
        } else if (user != null) {
            username.setText(user.getDisplayName());
            loadPic(user.getAvatarUrl());
            toolbarLayout.setOnClickListener(v ->
                    startActivity(new Intent(this, ContactDetailsActivity.class).putExtra(ContactDetailsActivity.USER, user))
            );
        }
    }

    private void loadPic(String url) {
        if (!url.isEmpty())
            Picasso.get().load(url).placeholder(R.drawable.placeholder).into(userPic);
        else
            Picasso.get().load(R.drawable.placeholder).placeholder(R.drawable.placeholder).into(userPic);
    }
}
