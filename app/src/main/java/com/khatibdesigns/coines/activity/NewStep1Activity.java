package com.khatibdesigns.coines.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khatibdesigns.coines.R;

import androidx.appcompat.app.AppCompatActivity;

public class NewStep1Activity extends AppCompatActivity {

    private LinearLayout nextPage;
    private TextView txtSell, txtSellCoinesFee, txtSellNetworkFee, txtSellNet, txtRcv, txtRcvCoinesFee, txtRcvNetworkFee, txtRcvNet, importantNotice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_step1);

        nextPage = findViewById(R.id.next_page);
        txtSell = findViewById(R.id.txt_sell);
        txtSellCoinesFee = findViewById(R.id.txt_sell_coines_fee);
        txtSellNetworkFee = findViewById(R.id.txt_sell_network_fee);
        txtSellNet = findViewById(R.id.txt_sell_net);
        txtRcv = findViewById(R.id.txt_rcv);
        txtRcvCoinesFee = findViewById(R.id.txt_rcv_coines_fee);
        txtRcvNetworkFee = findViewById(R.id.txt_rcv_network_fee);
        txtRcvNet = findViewById(R.id.txt_rcv_net);
        importantNotice = findViewById(R.id.important_notice);
        init();

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nextPage.setOnClickListener(view -> startActivity(new Intent(this, NewStep2Activity.class)));
    }

    private void init() {
        String youSell = "You sell*";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(youSell);
        builder.setSpan(new ForegroundColorSpan(Color.parseColor("#00C5F1")), youSell.length() - 1, youSell.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtSell.setText(builder);

        String coinesFees = "Coines.io fee:";
        String feeAmount = " 0.3500 LTP (0.01%)";
        builder = new SpannableStringBuilder();
        builder.append(coinesFees);
        builder.append(feeAmount);
        builder.setSpan(new StyleSpan(Typeface.BOLD), 0, coinesFees.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtSellCoinesFee.setText(builder);

        String networkFee = "Network fee:";
        feeAmount = " 1.0000 LTP";
        builder = new SpannableStringBuilder();
        builder.append(networkFee);
        builder.append(feeAmount);
        builder.setSpan(new StyleSpan(Typeface.BOLD), 0, networkFee.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtSellNetworkFee.setText(builder);

        String net = "Net:";
        String netAmount = " 3487.5000 LTP";
        builder = new SpannableStringBuilder();
        builder.append(net);
        builder.append(netAmount);
        builder.setSpan(new StyleSpan(Typeface.BOLD), 0, net.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtSellNet.setText(builder);

        String youReceive = "You receive*";
        builder = new SpannableStringBuilder();
        builder.append(youReceive);
        builder.setSpan(new ForegroundColorSpan(Color.parseColor("#00C5F1")), youReceive.length() - 1, youReceive.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtRcv.setText(builder);

        feeAmount = " 0.1000 RPL (0.01%)";
        builder = new SpannableStringBuilder();
        builder.append(coinesFees);
        builder.setSpan(new StyleSpan(Typeface.BOLD), 0, coinesFees.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(feeAmount);
        txtRcvCoinesFee.setText(builder);

        netAmount = " 0.1000 RPL";
        builder = new SpannableStringBuilder();
        builder.append(networkFee);
        builder.setSpan(new StyleSpan(Typeface.BOLD), 0, networkFee.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(netAmount);
        txtRcvNetworkFee.setText(builder);

        netAmount = " 8.9000 RPL";
        builder = new SpannableStringBuilder();
        builder.append(net);
        builder.setSpan(new StyleSpan(Typeface.BOLD), 0, net.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(netAmount);
        txtRcvNet.setText(builder);

        String important = "Important: ";
        builder = new SpannableStringBuilder();
        builder.append(getString(R.string.important_notice));
        builder.setSpan(new StyleSpan(Typeface.BOLD), 0, important.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        importantNotice.setText(builder);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem notification = menu.findItem(R.id.action_notifications);
        notification.getActionView().setOnClickListener(view -> startActivity(new Intent(this, NotificationsActivity.class)));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
