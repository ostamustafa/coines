package com.khatibdesigns.coines.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;

import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.util.model.Notification;
import com.khatibdesigns.coines.util.adapter.NotificationAdapter;

import java.util.ArrayList;

public class NotificationsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RecyclerView notifications = findViewById(R.id.notifications_list);
        ArrayList<Notification> notificationList = new ArrayList<>();
        notificationList.add(new Notification(
                "The status of your exchange has changed.",
                "10.0000 (RPL) -  3500.0000 (LTP)",
                "July 10, 21:44"));
        notificationList.add(new Notification(
                "The time for completing your exchange has expired and it is no longer valid.",
                "10.0000 (RPL) -  3500.0000 (LTP)",
                "July 10, 21:44"));
        notificationList.add(new Notification(
                "Your exchange offer has been declined by the other side.",
                "10.0000 (RPL) -  3500.0000 (LTP)",
                "July 10, 21:44"));
        notificationList.add(new Notification(
                "Your exchange has been completed successfully.",
                "10.0000 (RPL) -  3500.0000 (LTP)",
                "July 10, 21:44"));
        notificationList.add(new Notification(
                "Your exchange offer has been declined by the other side.",
                "10.0000 (RPL) -  3500.0000 (LTP)",
                "July 10, 21:44"));
        notificationList.add(new Notification(
                "The time for completing your exchange has expired and it is no longer valid.",
                "10.0000 (RPL) -  3500.0000 (LTP)",
                "July 10, 21:44"));

        NotificationAdapter adapter = new NotificationAdapter(this, notificationList);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        notifications.setLayoutManager(llm);
        notifications.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
