package com.khatibdesigns.coines.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.util.database.AppDatabase;
import com.khatibdesigns.coines.util.model.User;
import com.onesignal.OneSignal;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;
import java.util.Set;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    private boolean first;
    private EditText displayName, edtUsername;
    private FirebaseFirestore db;
    private FirebaseUser user;
    private ProgressBar wait, loading;
    private String id, username;
    private CircleImageView avatar;

    public static final String FIRST = ProfileActivity.class.getName() + ".FIRST";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        ActionBar bar = getSupportActionBar();
        displayName = findViewById(R.id.display_name);
        edtUsername = findViewById(R.id.username);
        first = getIntent().getBooleanExtra(FIRST, false);
        db = FirebaseFirestore.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();
        wait = findViewById(R.id.wait);
        loading = findViewById(R.id.loading);
        avatar = findViewById(R.id.avatar);
        TextView changeAvatar = findViewById(R.id.change_picture);

        if (!first) {
            FirebaseStorage storage = FirebaseStorage.getInstance();
            storage.getReference("avatars")
                    .child(user.getUid())
                    .child("avatar.jpg")
                    .getDownloadUrl()
                    .addOnSuccessListener(uri ->
                            Picasso.get().load(uri).error(R.drawable.placeholder).into(avatar)
                    );
        }

        View.OnClickListener listener = view -> {
            ImagePicker.create(this)
                    .returnMode(ReturnMode.ALL)
                    .folderMode(true)
                    .toolbarImageTitle("Tap to select") // image selection title
                    .toolbarArrowColor(Color.WHITE) // Toolbar 'up' arrow color
                    .includeVideo(false) // Show video on image picker
                    .single()
                    .limit(1) // max images can be selected (99 by default)
                    .showCamera(true) // show camera or not (true by default)
                    .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                    .enableLog(false) // disabling log
                    .start();
            loading.setVisibility(View.VISIBLE);
        };
        changeAvatar.setOnClickListener(listener);
        avatar.setOnClickListener(listener);

        if (!first) {
            if (bar != null)
                bar.setDisplayHomeAsUpEnabled(true);
            if (user != null) {
                db.collection("users")
                        .document(user.getUid())
                        .get()
                        .addOnSuccessListener(success -> {
                            username = success.getString("username");
                            displayName.setText(success.getString("display_name"));
                            edtUsername.setText(username);
                            edtUsername.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
                        })
                        .addOnFailureListener(failure ->
                                Log.d("Query", "Exception: " + failure)
                        );
            }
        }

        edtUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0 && !s.toString().equals(username)) {
                    wait.setVisibility(View.VISIBLE);
                    edtUsername.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    db.collection("users")
                            .whereEqualTo("username", s.toString())
                            .get()
                            .addOnSuccessListener(success -> {
                                if (success.isEmpty()) {
                                    wait.setVisibility(View.GONE);
                                    if (!s.toString().equals(username) && s.length() != 0)
                                        edtUsername.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.completed, 0);
                                } else {
                                    wait.setVisibility(View.GONE);
                                    if (!s.toString().equals(username) && s.length() != 0)
                                        edtUsername.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.failed, 0);
                                }
                            });
                } else {
                    wait.setVisibility(View.GONE);
                    edtUsername.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty() && s.toString().equals(username))
                    edtUsername.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
            }
        });

        findViewById(R.id.btn_save_details).setOnClickListener(view -> {
            if (edtUsername.getText().toString().trim().isEmpty()) {
                edtUsername.setError("Username cannot be empty");
                edtUsername.requestFocus();
                return;
            }
            OneSignal.idsAvailable((userId, registrationId) ->
                    id = userId
            );
            HashMap<String, Object> data = new HashMap<>();
            data.put("display_name", displayName.getText().toString());
            data.put("username", edtUsername.getText().toString());
            data.put("push_token", id);
            data.put("online_status", true);
            data.put("phone_number", Objects.requireNonNull(user).getPhoneNumber());

            User me = AppDatabase.getDatabase(this).userDao().getUser(user.getUid());
            if (me != null) {
                me.setDisplayName(displayName.getText().toString());
                me.setUsername(edtUsername.getText().toString());
                data.put("avatar_url", me.getAvatarUrl());
                AppDatabase.getDatabase(this).userDao().update(me);
            } else {
                me = new User(
                        user.getUid(),
                        displayName.getText().toString(),
                        edtUsername.getText().toString(),
                        "",
                        user.getPhoneNumber(),
                        true,
                        id,
                        true
                );
                AppDatabase.getDatabase(this).userDao().insert(me);
            }
            AppDatabase.destroyInstance();

            if (first) {
                db.collection("users")
                        .document(Objects.requireNonNull(user).getUid())
                        .set(data, SetOptions.merge());
                startActivity(new Intent(this, MainActivity.class));
                finish();
            } else
                db.collection("users")
                        .document(Objects.requireNonNull(user).getUid())
                        .update(data);
            this.onBackPressed();
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            Image image = ImagePicker.getFirstImageOrNull(data);
            upload(image);
        }
    }

    private void upload(Image image) {
        if (image != null) {
            try {
                byte[] bytes = getBytes(image);
                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference avatars = storage.getReference("avatars");
                avatars.child(user.getUid())
                        .child("avatar.jpg")
                        .putBytes(bytes)
                        .addOnSuccessListener(success ->
                                success.getStorage().getDownloadUrl().addOnSuccessListener(uri -> {
                                    Picasso.get().load(uri).error(R.drawable.placeholder).into(avatar);
                                    loading.setVisibility(View.GONE);
                                    HashMap<String, String> data = new HashMap<>();
                                    data.put("avatar_url", uri.toString());
                                    db.collection("users")
                                            .document(user.getUid())
                                            .set(data, SetOptions.mergeFields("avatar_url"));
                                    User me = AppDatabase.getDatabase(ProfileActivity.this).userDao().getUser(user.getUid());
                                    if (me != null) {
                                        me.setAvatarUrl(uri.toString());
                                        AppDatabase.getDatabase(ProfileActivity.this)
                                                .userDao()
                                                .update(me);
                                    }
                                })
                        )
                        .addOnFailureListener(failure -> {
                            Toast.makeText(this, "Image failed to load", Toast.LENGTH_SHORT).show();
                            loading.setVisibility(View.GONE);
                            Log.d("Query", "Exception: " + failure);
                        });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!first) {
            getMenuInflater().inflate(R.menu.menu_main, menu);
            MenuItem notification = menu.findItem(R.id.action_notifications);
            notification.getActionView().setOnClickListener(view -> startActivity(new Intent(this, NotificationsActivity.class)));
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private byte[] getBytes(Image image) throws IOException {
        Bitmap bitmap = BitmapFactory.decodeFile(image.getPath());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        byte[] data = out.toByteArray();
        out.close();
        return data;
    }
}
