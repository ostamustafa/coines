package com.khatibdesigns.coines.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.khatibdesigns.coines.R;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

public class SettingsActivity extends AppCompatActivity {

    private ImageView walletDelete1, walletDelete2, walletDelete3;
    private LinearLayout addNewWallet, btnChangeDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar bar = getSupportActionBar();
        if (bar != null)
            bar.setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_settings);
        walletDelete1 = findViewById(R.id.wallet_delete_1);
        walletDelete2 = findViewById(R.id.wallet_delete_2);
        walletDelete3 = findViewById(R.id.wallet_delete_3);
        addNewWallet = findViewById(R.id.add_new_wallet);
        btnChangeDetails = findViewById(R.id.btn_change_details);

        View.OnClickListener listener = v -> {
            MaterialDialog.Builder builder = new MaterialDialog.Builder(this);
            builder.title(R.string.delete_wallet)
                    .content("Are you sure you want to delete this wallet?")
                    .positiveColor(Color.parseColor("#DB0000"))
                    .positiveText("Delete")
                    .negativeText("Close")
                    .build()
                    .show();
        };
        walletDelete1.setOnClickListener(listener);
        walletDelete2.setOnClickListener(listener);
        walletDelete3.setOnClickListener(listener);

        addNewWallet.setOnClickListener(v -> startActivity(new Intent(this, AddWalletActivity.class)));

        btnChangeDetails.setOnClickListener(v ->
                startActivity(new Intent(this, ProfileActivity.class))
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem notification = menu.findItem(R.id.action_notifications);
        notification.getActionView().setOnClickListener(view -> startActivity(new Intent(this, NotificationsActivity.class)));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
