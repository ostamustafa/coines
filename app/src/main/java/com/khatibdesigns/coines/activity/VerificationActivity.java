package com.khatibdesigns.coines.activity;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;

import com.hbb20.CountryCodePicker;
import com.khatibdesigns.coines.R;

public class VerificationActivity extends AppCompatActivity {

    private CountryCodePicker ccp;
    private EditText phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        ccp = findViewById(R.id.ccp);
        phoneNumber = findViewById(R.id.phone_number);
        findViewById(R.id.btn_send_code).setOnClickListener(v -> {
            if (phoneNumber.getText().toString().trim().isEmpty()) {
                Snackbar.make(v, "Please enter a phone number", Snackbar.LENGTH_SHORT).show();
                return;
            }
            String number = ccp.getFullNumberWithPlus() + phoneNumber.getText().toString();
            Log.d("Query", "phone number: " + number);
            startActivity(new Intent(this, VerifyCodeActivity.class).putExtra(VerifyCodeActivity.PHONE_NUMBER, number));
        });
    }
}
