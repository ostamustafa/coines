package com.khatibdesigns.coines.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.khatibdesigns.coines.R;

import java.util.concurrent.TimeUnit;

public class VerifyCodeActivity extends AppCompatActivity {

    private EditText code1, code2, code3, code4, code5, code6;
    private TextView timer;
    private Button verifyCode;
    private String phoneNumber;

    public static final String PHONE_NUMBER = VerifyCodeActivity.class.getName() + ".PHONE_NUMBER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_code);
        ActionBar bar = getSupportActionBar();
        if (bar != null)
            bar.setDisplayHomeAsUpEnabled(true);

        timer = findViewById(R.id.txt_send_code);
        code1 = findViewById(R.id.edt_code_1);
        code2 = findViewById(R.id.edt_code_2);
        code3 = findViewById(R.id.edt_code_3);
        code4 = findViewById(R.id.edt_code_4);
        code5 = findViewById(R.id.edt_code_5);
        code6 = findViewById(R.id.edt_code_6);
        verifyCode = findViewById(R.id.btn_verify_code);
        phoneNumber = getIntent().getStringExtra(PHONE_NUMBER);

        verifyCode.setOnClickListener(view ->
                Toast.makeText(this, "Please enter code first", Toast.LENGTH_SHORT).show()
        );

        new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (timer.hasOnClickListeners())
                    timer.setOnClickListener(null);
                SpannableStringBuilder sendCode = new SpannableStringBuilder();
                sendCode.append("Didn\'t receive a code?\nPlease wait ");
                int length = sendCode.length() - 1;
                sendCode.setSpan(new ForegroundColorSpan(Color.parseColor("#8B8B8B")), 0, length, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

                sendCode.append("0:");
                if (millisUntilFinished / 1000 < 10)
                    sendCode.append("0");
                sendCode.append(String.valueOf(millisUntilFinished / 1000));
                sendCode.setSpan(new ForegroundColorSpan(Color.parseColor("#15A6E6")), length, sendCode.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                timer.setText(sendCode);
                timer.setGravity(Gravity.CENTER);
            }

            @Override
            public void onFinish() {
                timer.setText(R.string.send_code_again);
                timer.setOnClickListener(view -> {
                    sendCode(phoneNumber);
                    this.start();
                });
            }
        }.start();

        sendCode(phoneNumber);
    }

    private void sendCode(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,
                60,
                TimeUnit.SECONDS,
                this,
                new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                        String code = phoneAuthCredential.getSmsCode();
                        Log.d("Query", "SMS Code: " + code);
                        if (code != null) {
                            code1.setText(String.valueOf(code.charAt(0)));
                            code2.setText(String.valueOf(code.charAt(1)));
                            code3.setText(String.valueOf(code.charAt(2)));
                            code4.setText(String.valueOf(code.charAt(3)));
                            code5.setText(String.valueOf(code.charAt(4)));
                            code6.setText(String.valueOf(code.charAt(5)));
                            code6.requestFocus();
                        }
                        verifyCode.setOnClickListener(view ->
                                signInWithPhoneAuthCredential(phoneAuthCredential)
                        );
                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e) {
                        e.printStackTrace();
                        Log.d("Query", "Exception: " + e);
                    }

                    @Override
                    public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                        super.onCodeSent(verificationId, forceResendingToken);
                        verifyCode.setOnClickListener(view -> {
                            String code = code1.getText().toString() +
                                    code2.getText().toString() +
                                    code3.getText().toString() +
                                    code4.getText().toString() +
                                    code5.getText().toString() +
                                    code6.getText().toString();
                            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
                            signInWithPhoneAuthCredential(credential);
                        });
                    }
                }
        );
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        FirebaseAuth.getInstance()
                .signInWithCredential(credential)
                .addOnFailureListener(failure -> Log.d("Query", "Exception: " + failure))
                .addOnSuccessListener(success -> startActivity(new Intent(this, ProfileActivity.class).putExtra(ProfileActivity.FIRST, true)));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
