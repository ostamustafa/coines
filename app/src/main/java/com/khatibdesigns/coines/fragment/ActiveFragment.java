package com.khatibdesigns.coines.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.activity.ExchangeStatusActivity;
import com.khatibdesigns.coines.util.adapter.ExchangeAdapter;
import com.khatibdesigns.coines.util.model.Exchange;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ActiveFragment extends Fragment {

    private RecyclerView rclActive;

    public static ActiveFragment newInstance() {
        ActiveFragment fragment = new ActiveFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_active, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rclActive = view.findViewById(R.id.active_list);
        ArrayList<Exchange> activeList = new ArrayList<>();
        activeList.add(new Exchange(
                "",
                "",
                "3500.000 (LTP)",
                "10.000 (RPL)",
                "2d 3h 12min left to complete",
                null));
        activeList.add(new Exchange(
                "",
                "",
                "3500.000 (LTP)",
                "10.000 (RPL)",
                "2d 3h 12min left to complete",
                null));
        activeList.add(new Exchange(
                "",
                "",
                "3500.000 (LTP)",
                "10.000 (RPL)",
                "2d 3h 12min left to complete",
                null));
        ExchangeAdapter activeAdapter = new ExchangeAdapter(getContext(), activeList, (v, position) -> {
            Exchange exchange = activeList.get(position);
            startActivity(new Intent(getContext(), ExchangeStatusActivity.class).putExtra(ExchangeStatusActivity.EXCHANGE, exchange));
        });
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(RecyclerView.VERTICAL);
        rclActive.setLayoutManager(llm);
        rclActive.setAdapter(activeAdapter);
    }
}
