package com.khatibdesigns.coines.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.activity.MessagesActivity;
import com.khatibdesigns.coines.util.database.AppDatabase;
import com.khatibdesigns.coines.util.model.Dialog;
import com.khatibdesigns.coines.util.model.Message;
import com.khatibdesigns.coines.util.model.User;
import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.dialogs.DialogsList;
import com.stfalcon.chatkit.dialogs.DialogsListAdapter;
import com.stfalcon.chatkit.utils.DateFormatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class ChatsFragment extends Fragment implements DateFormatter.Formatter {

    private DialogsList dialogsList;
    private AppDatabase db;
    private User me;
    private DialogsListAdapter<Dialog> adapter;
    private FirebaseFirestore firestore;

    public ChatsFragment() {
    }

    public static ChatsFragment newInstance() {
        ChatsFragment fragment = new ChatsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chats, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        populate();
    }

    private void init(View view) {
        dialogsList = view.findViewById(R.id.dialogsList);
        db = AppDatabase.getDatabase(getContext());
        firestore = FirebaseFirestore.getInstance();
        me = db.userDao().getUser(FirebaseAuth.getInstance().getCurrentUser().getUid());
        adapter = new DialogsListAdapter<>(R.layout.layout_dialog, ((imageView, url, payload) -> {
            if (url == null || url.isEmpty())
                Picasso.get().load(R.drawable.placeholder).into(imageView);
            else
                Picasso.get().load(url).error(R.drawable.placeholder).placeholder(R.drawable.placeholder).into(imageView);
        }));
        adapter.setItems(new ArrayList<>());
        dialogsList.setAdapter(adapter);
        adapter.setOnDialogClickListener(dialog ->
                startActivity(new Intent(getContext(), MessagesActivity.class).putExtra(MessagesActivity.DIALOG, dialog))
        );
    }

    private void populate() {
        DocumentReference meRef = firestore.collection("users").document(FirebaseAuth.getInstance().getCurrentUser().getUid());
        firestore.collection("dialogs")
                .whereArrayContains("members", meRef)
                .orderBy("created_at")
                .get()
                .addOnSuccessListener(success -> {
                    for (QueryDocumentSnapshot snapshot : success) {
                        Date createdAt = snapshot.getDate("created_at");
                        String title, url;
                        ArrayList<User> users = new ArrayList<>();
                        ArrayList<DocumentReference> usersRef = (ArrayList<DocumentReference>) snapshot.get("members");
                        if (usersRef.size() <= 2) {
                            User user = db.userDao().getUser(usersRef.get(0).getId());
                            if (user.getId().equals(me.getId()))
                                user = db.userDao().getUser(usersRef.get(1).getId());
                            title = user.getDisplayName();
                            url = user.getAvatarUrl();
                            users.add(user);
                            users.add(me);
                            User finalUser = user;
                            firestore.collection("messages")
                                    .whereEqualTo("dialog_id", snapshot.getReference())
                                    .orderBy("created_at", Query.Direction.DESCENDING)
                                    .limit(1)
                                    .get()
                                    .addOnSuccessListener(success1 -> {
                                        DocumentSnapshot msgSnapshot = success1.getDocuments().get(0);
                                        User sender = msgSnapshot.getDocumentReference("sender_id").getId().equals(me.getId()) ? me : finalUser;
                                        Message message = new Message(
                                                msgSnapshot.getId(),
                                                msgSnapshot.getString("message"),
                                                sender,
                                                snapshot.getId(),
                                                msgSnapshot.getDate("created_at"));
                                        Dialog dialog = new Dialog(snapshot.getId(), url, title, users, message, 0, createdAt);
                                        db.dialogDao().insert(dialog);
                                        db.messageDao().insert(message);
                                        adapter.upsertItem(dialog);
                                    });
                        } else {

                        }
                    }
                });
    }

    @Override
    public String format(Date date) {
        if (DateFormatter.isToday(date)) {
            return DateFormatter.format(date, DateFormatter.Template.TIME);
        } else if (DateFormatter.isYesterday(date)) {
            return "Yesterday";
        } else if (sameWeek(date)) {
            return DateFormatter.format(date, "EEEE");
        } else if (DateFormatter.isCurrentYear(date)) {
            return DateFormatter.format(date, "dd MMM");
        } else {
            return DateFormatter.format(date, "dd MMM yyyy");
        }
    }

    private boolean sameWeek(Date date) {
        Calendar now = Calendar.getInstance();
        Calendar current = Calendar.getInstance();
        current.setTime(date);
        return now.get(Calendar.WEEK_OF_YEAR) == current.get(Calendar.WEEK_OF_YEAR);
    }
}
