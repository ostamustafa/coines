package com.khatibdesigns.coines.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.hbb20.CountryCodePicker;
import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.activity.AddContactActivity;
import com.khatibdesigns.coines.activity.MessagesActivity;
import com.khatibdesigns.coines.util.adapter.ContactsAdapter;
import com.khatibdesigns.coines.util.database.AppDatabase;
import com.khatibdesigns.coines.util.model.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class ContactsFragment extends Fragment {

    private ArrayList<User> onlineList, offlineList;
    private RecyclerView online, offline;
    private AppDatabase db;
    private CountryCodePicker picker;
    private SwipeRefreshLayout swipe;
    private User me;

    public static ContactsFragment newInstance() {
        ContactsFragment fragment = new ContactsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contacts, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        db = AppDatabase.getDatabase(getContext());
        me = db.userDao().getUser(FirebaseAuth.getInstance().getCurrentUser().getUid());
        online = view.findViewById(R.id.online);
        offline = view.findViewById(R.id.offline);
        picker = view.findViewById(R.id.ccp);
        FloatingActionButton addContact = view.findViewById(R.id.add_contact);
        swipe = view.findViewById(R.id.swipe);
        onlineList = (ArrayList<User>) db.userDao().getStatus(true, me.getId());
        if (onlineList == null)
            onlineList = new ArrayList<>();
        offlineList = (ArrayList<User>) db.userDao().getStatus(false, me.getId());
        if (offlineList == null)
            offlineList = new ArrayList<>();

        swipe.setOnRefreshListener(this::getContactList);

        addContact.setOnClickListener(v -> startActivity(new Intent(getContext(), AddContactActivity.class)));
        if (Objects.requireNonNull(getContext()).checkCallingOrSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()), new String[]{Manifest.permission.READ_CONTACTS}, 1000);
        else {
            init();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        onlineList.clear();
        onlineList.addAll(db.userDao().getStatus(true, me.getId()));
        offlineList.clear();
        offlineList.addAll(db.userDao().getStatus(false, me.getId()));
        if (online.getAdapter() != null)
            online.getAdapter().notifyDataSetChanged();
        if (offline.getAdapter() != null)
            offline.getAdapter().notifyDataSetChanged();
    }

    private void getContactList() {
        class Contact {
            public String name;
            // use can use a HashSet here to avoid duplicate phones per contact
            private List<String> phones = new ArrayList<>();
        }

        db = AppDatabase.getDatabase(getContext());
        String[] projection = new String[]{
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER
        };
        Cursor cursor = Objects.requireNonNull(getContext()).getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                projection,
                null,
                null,
                null
        );

        @SuppressLint("UseSparseArrays")
        HashMap<Long, Contact> contacts = new HashMap<>();

        if (cursor != null) {
            while (cursor.moveToNext()) {
                long id = cursor.getLong(cursor.getColumnIndex(projection[0]));
                String name = cursor.getString(cursor.getColumnIndex(projection[1]));
                String phone = cursor.getString(cursor.getColumnIndex(projection[2]));

                Contact c = contacts.get(id);
                if (c == null) {
                    c = new Contact();
                    c.name = name;
                    contacts.put(id, c);
                }
                c.phones.add(phone);
            }
            cursor.close();
        }

        List<Contact> values = new ArrayList<>(contacts.values());
        Collections.sort(values, (a, b) -> a.name.compareTo(b.name));
        User me = db.userDao().getUser(FirebaseAuth.getInstance().getCurrentUser().getUid());
        for (Contact contact : values)
            for (String number : contact.phones) {
                picker.setAutoDetectedCountry(true);
                String locale = picker.getSelectedCountryNameCode();
                String phoneNumber = number.replace(" ", "");
                PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                if (!number.contains("+")) {
                    try {
                        PhoneNumber pN = phoneUtil.parse(number, locale);
                        if (phoneUtil.isValidNumberForRegion(pN, locale))
                            phoneNumber = phoneUtil.format(pN, PhoneNumberUtil.PhoneNumberFormat.E164);
                    } catch (NumberParseException e) {
                        e.printStackTrace();
                    }
                }
                if (!phoneNumber.equals(me.getPhoneNumber())) {
                    FirebaseFirestore firestore = FirebaseFirestore.getInstance();
                    firestore.collection("users")
                            .whereEqualTo("phone_number", phoneNumber)
                            .orderBy("display_name")
                            .get()
                            .addOnSuccessListener(success -> {
                                for (QueryDocumentSnapshot snapshot : success) {
                                    boolean exists = db.userDao().exists(snapshot.getId());
                                    User current = db.userDao().getUser(snapshot.getId());
                                    if (exists) {
                                        if (current.isMain()) {
                                            User user = new User(
                                                    snapshot.getId(),
                                                    snapshot.getString("display_name"),
                                                    snapshot.getString("username"),
                                                    snapshot.getString("avatar_url"),
                                                    snapshot.getString("phone_number"),
                                                    snapshot.getBoolean("online_status"),
                                                    snapshot.getString("push_token"),
                                                    true
                                            );
                                            db.userDao().insert(user);
                                        }
                                    } else {
                                        User user = new User(
                                                snapshot.getId(),
                                                snapshot.getString("display_name"),
                                                snapshot.getString("username"),
                                                snapshot.getString("avatar_url"),
                                                snapshot.getString("phone_number"),
                                                snapshot.getBoolean("online_status"),
                                                snapshot.getString("push_token"),
                                                true
                                        );
                                        db.userDao().insert(user);
                                    }
                                }
                                onlineList.clear();
                                offlineList.clear();
                                onlineList.addAll(db.userDao().getStatus(true, me.getId()));
                                offlineList.addAll(db.userDao().getStatus(false, me.getId()));
                                online.getAdapter().notifyDataSetChanged();
                                offline.getAdapter().notifyDataSetChanged();
                                swipe.setRefreshing(false);
                            });
                }
            }
    }

    private void init() {
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        manager.setOrientation(RecyclerView.VERTICAL);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(RecyclerView.VERTICAL);

        ContactsAdapter onlineAdapter = new ContactsAdapter(getContext(), onlineList, (v, position) -> {
            User user = onlineList.get(position);
            startActivity(new Intent(getContext(), MessagesActivity.class).putExtra(MessagesActivity.USER, user));
        });
        ContactsAdapter offlineAdapter = new ContactsAdapter(getContext(), offlineList, (v, position) -> {
            User user = offlineList.get(position);
            startActivity(new Intent(getContext(), MessagesActivity.class).putExtra(MessagesActivity.USER, user));
        });

        online.setAdapter(onlineAdapter);
        offline.setAdapter(offlineAdapter);
        online.setLayoutManager(manager);
        offline.setLayoutManager(llm);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1000) {
            getContactList();
            init();
        }
    }
}