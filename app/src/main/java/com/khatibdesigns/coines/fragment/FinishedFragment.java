package com.khatibdesigns.coines.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.activity.ExchangeStatusActivity;
import com.khatibdesigns.coines.util.adapter.ExchangeAdapter;
import com.khatibdesigns.coines.util.model.Exchange;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class FinishedFragment extends Fragment {

    private RecyclerView rclFinished;

    public static FinishedFragment newInstance() {
        FinishedFragment fragment = new FinishedFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_finished, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rclFinished = view.findViewById(R.id.finished_list);
        ArrayList<Exchange> finishedList = new ArrayList<>();
        finishedList.add(new Exchange(
                "",
                "",
                "3500.000 (LTP)",
                "10.000 (RPL)",
                null,
                getString(R.string.completed)));
        finishedList.add(new Exchange(
                "",
                "",
                "3500.000 (LTP)",
                "10.000 (RPL)",
                null,
                getString(R.string.completed)));
        finishedList.add(new Exchange(
                "",
                "",
                "3500.000 (LTP)",
                "10.000 (RPL)",
                null,
                getString(R.string.failed)));
        ExchangeAdapter finishedAdapter = new ExchangeAdapter(getContext(), finishedList, (v, position) -> {
            Exchange exchange = finishedList.get(position);
            startActivity(new Intent(getContext(), ExchangeStatusActivity.class).putExtra(ExchangeStatusActivity.EXCHANGE, exchange));
        });
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(RecyclerView.VERTICAL);
        rclFinished.setLayoutManager(llm);
        rclFinished.setAdapter(finishedAdapter);
    }
}
