package com.khatibdesigns.coines.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.util.model.Dialog;
import com.khatibdesigns.coines.util.model.Message;
import com.stfalcon.chatkit.dialogs.DialogsList;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class GroupsFragment extends Fragment {

    public GroupsFragment() {
    }

    public static GroupsFragment newInstance() {
        GroupsFragment fragment = new GroupsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_groups, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DialogsList group = view.findViewById(R.id.group_dialogs);

        ArrayList<Dialog> dialogs = new ArrayList<>();
//        Message privateMessage = new Message("private", "Private Group", null, null);

//        dialogs.add(new Dialog(
//                "support",
//                null,
//                "Coines.io Support",
//                Arrays.asList(new User[35121]),
//                new Message("public", "Public Group", null, null),
//                3
//        ));
//
//        dialogs.add(new Dialog(
//                "private",
//                null,
//                "My Private Group",
//                Arrays.asList(new User[4]),
//                privateMessage,
//                1
//        ));
//
//        dialogs.add(new Dialog(
//                "bitcoin",
//                null,
//                "Bitcoin Exchanges",
//                Arrays.asList(new User[121]),
//                privateMessage,
//                0
//        ));
//
//        dialogs.add(new Dialog(
//                "exchange",
//                null,
//                "Best Exchange Offers",
//                Arrays.asList(new User[3512]),
//                privateMessage,
//                0
//        ));

//        DialogsListAdapter<Dialog> adapter = new DialogsListAdapter<>(R.layout.layout_dialog, GroupDialogViewHolder.class, null);
//        adapter.addItems(dialogs);
//        group.setAdapter(adapter);
    }
}
