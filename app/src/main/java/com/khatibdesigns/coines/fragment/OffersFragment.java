package com.khatibdesigns.coines.fragment;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.activity.ExchangeStatusActivity;
import com.khatibdesigns.coines.util.adapter.ExchangeAdapter;
import com.khatibdesigns.coines.util.model.Exchange;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class OffersFragment extends Fragment {

    private RecyclerView rclOffers;
    private LinearLayout addNewOffer;

    public static OffersFragment newInstance() {
        OffersFragment fragment = new OffersFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_offers, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rclOffers = view.findViewById(R.id.offers_list);
        addNewOffer = view.findViewById(R.id.add_new_offer);

        addNewOffer.setOnClickListener((v) -> {
            if (getContext() != null && getView() != null) {
                Context context = getContext();
                ViewGroup parent = (ViewGroup) getView().getParent();
                LinearLayout input = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.new_offer_input, parent, false);
                ImageView paste = input.findViewById(R.id.btn_paste);
                EditText link = input.findViewById(R.id.edt_link);
                paste.setOnClickListener((v1 -> {
                    ClipboardManager manager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                    if (manager != null && manager.getPrimaryClipDescription() != null && manager.getPrimaryClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                        ClipData.Item item = manager.getPrimaryClip().getItemAt(0);
                        String copiedText = item.getText().toString();
                        link.setText(copiedText);
                    }
                }));
                new MaterialDialog.Builder(context)
                        .title("Add New Offer")
                        .customView(input, true)
                        .positiveColor(Color.parseColor("#3CB95D"))
                        .positiveText("Add")
                        .onPositive(((dialog, which) -> {

                        }))
                        .negativeColor(Color.BLACK)
                        .negativeText("Cancel")
                        .cancelable(true)
                        .build().show();
            }
        });

        ArrayList<Exchange> offersList = new ArrayList<>();
        offersList.add(new Exchange(
                "",
                "",
                "3500.000 (LTP)",
                "10.000 (RPL)",
                null,
                getString(R.string.tap_details)));
        offersList.add(new Exchange(
                "",
                "",
                "3500.000 (LTP)",
                "10.000 (RPL)",
                null,
                getString(R.string.tap_details)));
        offersList.add(new Exchange(
                "",
                "",
                "3500.000 (LTP)",
                "10.000 (RPL)",
                null,
                getString(R.string.tap_details)));
        ExchangeAdapter offersAdapter = new ExchangeAdapter(getContext(), offersList, (v, position) -> {
            Exchange exchange = offersList.get(position);
            startActivity(new Intent(getContext(), ExchangeStatusActivity.class).putExtra(ExchangeStatusActivity.EXCHANGE, exchange));
        });
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(RecyclerView.VERTICAL);
        rclOffers.setLayoutManager(llm);
        rclOffers.setAdapter(offersAdapter);
    }
}
