package com.khatibdesigns.coines.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.util.adapter.RequestAdapter;
import com.khatibdesigns.coines.util.database.AppDatabase;
import com.khatibdesigns.coines.util.listener.RequestCallback;
import com.khatibdesigns.coines.util.model.Dialog;
import com.khatibdesigns.coines.util.model.Request;
import com.khatibdesigns.coines.util.model.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class RequestsFragment extends Fragment implements RequestCallback {

    private AppDatabase db;
    private FirebaseFirestore firestore;
    private RecyclerView requests;
    private RequestAdapter adapter;
    private FirebaseUser me;
    private ArrayList<Request> requestList;
    private LinearLayout declineAll;

    public static RequestsFragment newInstance() {
        RequestsFragment fragment = new RequestsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_requests, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        declineAll.setOnClickListener(v -> {
            firestore.collection("requests")
                    .whereArrayContains("members", me.getUid())
                    .get()
                    .addOnSuccessListener(success -> {
                        for (DocumentSnapshot snapshot : success.getDocuments()) {
                            if (!snapshot.getDocumentReference("sender_id").getId().equals(me.getUid())) {
                                snapshot.getReference().delete();
                                Request request = db.requestDao().get(snapshot.getId());
                                adapter.delete(request);
                            }
                        }
                    });
            db.requestDao().declineAll();
        });

        DocumentReference refMe = firestore.collection("users").document(me.getUid());
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        manager.setOrientation(RecyclerView.VERTICAL);
        requests.setLayoutManager(manager);
        requests.setAdapter(adapter);

        firestore.collection("requests")
                .whereArrayContains("members", refMe)
                .orderBy("created_at")
                .get()
                .addOnSuccessListener(success -> {
                    db.requestDao().empty();
                    adapter.clear();
                    for (DocumentSnapshot snapshot : success) {
                        String collection = snapshot.getDocumentReference("sender_id").getParent().getPath();
                        if (collection.equals("users"))
                            populateUserRequest(snapshot);
                        else
                            populateDialogRequest(snapshot);
                    }
                });
    }

    @SuppressWarnings("All")
    private void populateDialogRequest(DocumentSnapshot snapshot) {
        DocumentReference dialogRef = snapshot.getDocumentReference("sender_id");
        FirebaseUser me = FirebaseAuth.getInstance().getCurrentUser();
        String previewMessage = snapshot.getString("preview_message");
        Date createdAt = snapshot.getTimestamp("created_at").toDate();
        ArrayList<DocumentReference> members = (ArrayList<DocumentReference>) snapshot.get("members");
        DocumentReference partnerRef = null;
        members = members == null ? new ArrayList<>() : members;
        for (DocumentReference reference : members)
            if (!reference.getId().equals(dialogRef.getId())) {
                partnerRef = reference;
                break;
            }
        final Dialog[] dialog = new Dialog[1];
        boolean selfSent = false;
        final Request[] request = new Request[1];
        if ((selfSent = db.dialogDao().exists(dialogRef.getId()))) {
            dialog[0] = db.dialogDao().getDialog(dialogRef.getId());
            User user = db.userDao().getUser(partnerRef.getId());
            previewMessage = "Invitation sent to " + user.getDisplayName();
            request[0] = new Request(snapshot.getId(), previewMessage, dialogRef.getId(), createdAt, true, false);
            db.dialogDao().insert(dialog[0]);
            db.requestDao().insert(request[0]);
            adapter.add(request[0]);
        } else {
            String finalPreviewMessage = previewMessage;
            dialogRef.get()
                    .addOnSuccessListener(success -> {
                        String dialogName = success.getString("title");
                        String avatarUrl = success.getString("avatar_url");
                        dialog[0] = new Dialog(success.getId(), avatarUrl, dialogName, new ArrayList<>(), null, 0, createdAt);
                        request[0] = new Request(snapshot.getId(), finalPreviewMessage, dialog[0].getId(), createdAt, false, false);
                        db.dialogDao().insert(dialog[0]);
                        db.requestDao().insert(request[0]);
                        adapter.add(request[0]);
                    });
        }
    }

    @SuppressWarnings("All")
    private void populateUserRequest(DocumentSnapshot snapshot) {
        DocumentReference senderRef = snapshot.getDocumentReference("sender_id");
        FirebaseUser me = FirebaseAuth.getInstance().getCurrentUser();
        String previewMessage = snapshot.getString("preview_message");
        Date createdAt = snapshot.getTimestamp("created_at").toDate();
        ArrayList<DocumentReference> members = (ArrayList<DocumentReference>) snapshot.get("members");
        DocumentReference partnerRef = null;
        members = members == null ? new ArrayList<>() : members;
        for (DocumentReference reference : members)
            if (!reference.getId().equals(senderRef.getId())) {
                partnerRef = reference;
                break;
            }
        boolean selfSent = me.getUid().equals(senderRef.getId());
        if (!selfSent)
            senderRef.get().addOnSuccessListener(success -> {
                User user = new User(success.getId(),
                        success.getString("display_name"),
                        success.getString("username"),
                        success.getString("avatar_url"),
                        success.getString("phone_number"),
                        success.getBoolean("online_status"),
                        success.getString("push_token"),
                        false);
                Request request = new Request(snapshot.getId(), previewMessage, user.getId(), createdAt, false, true);
                db.userDao().insert(user);
                db.requestDao().insert(request);
                adapter.add(request);
            });
        else {
            Request request = new Request(snapshot.getId(), previewMessage, partnerRef.getId(), createdAt, true, true);
            db.requestDao().insert(request);
            adapter.add(request);
        }
    }

    private void init(View view) {
        db = AppDatabase.getDatabase(getContext());
        firestore = FirebaseFirestore.getInstance();
        requests = view.findViewById(R.id.request_dialogs);
        requestList = (ArrayList<Request>) db.requestDao().getAll();
        adapter = new RequestAdapter(getContext(), requestList, this);
        me = FirebaseAuth.getInstance().getCurrentUser();
        declineAll = view.findViewById(R.id.btn_decline_all);
    }

    @Override
    public void accept(Request request) {
        if (request.isFromUser())
            acceptUser(request);
        else
            acceptGroup(request);
    }

    private void acceptUser(Request request) {
        DocumentReference user = firestore.collection("users").document(request.getReferenceId());
        DocumentReference me = firestore.collection("users").document(this.me.getUid());
        ArrayList<DocumentReference> members = new ArrayList<>();
        members.add(user);
        members.add(me);
        Timestamp createdAt = new Timestamp(new Date());
        HashMap<String, Object> data = new HashMap<>();
        data.put("members", members);
        data.put("created_at", createdAt);
        DocumentReference newDialog = firestore.collection("dialogs").document();
        newDialog.set(data)
                .addOnSuccessListener(success -> {
                    HashMap<String, Object> message = new HashMap<>();
                    message.put("created_at", new Timestamp(new Date()));
                    message.put("dialog_id", newDialog);
                    message.put("media_url", "");
                    message.put("message", request.getPreviewMessage());
                    message.put("sender_id", user);
                    firestore.collection("messages")
                            .document()
                            .set(message);
                });
        firestore.collection("requests").document(request.getId()).delete()
                .addOnSuccessListener(success -> adapter.delete(request));
    }

    private void acceptGroup(Request request) {
        firestore.collection("dialogs")
                .document(request.getReferenceId())
                .update("members", FieldValue.arrayUnion(firestore.collection("users").document(me.getUid())));
        firestore.collection("requests").document(request.getId()).delete();
        adapter.delete(request);
    }

    @Override
    public void decline(Request request) {
        FirebaseFirestore.getInstance().collection("requests")
                .document(request.getId())
                .delete();
        requestList.remove(request);
        adapter.notifyDataSetChanged();
    }
}
