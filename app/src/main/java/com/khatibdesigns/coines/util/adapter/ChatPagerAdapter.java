package com.khatibdesigns.coines.util.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.khatibdesigns.coines.fragment.ChatsFragment;
import com.khatibdesigns.coines.fragment.ContactsFragment;
import com.khatibdesigns.coines.fragment.GroupsFragment;
import com.khatibdesigns.coines.fragment.RequestsFragment;

public class ChatPagerAdapter extends FragmentPagerAdapter {

    public ChatPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ChatsFragment.newInstance();
            case 1:
                return GroupsFragment.newInstance();
            case 2:
                return RequestsFragment.newInstance();
            case 3:
                return ContactsFragment.newInstance();
            default:
                return ChatsFragment.newInstance();
        }
    }

    @Override
    public int getCount() {
        return 4;
    }
}
