package com.khatibdesigns.coines.util.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.util.database.AppDatabase;
import com.khatibdesigns.coines.util.listener.AdapterListener;
import com.khatibdesigns.coines.util.model.User;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.Item> {

    private Context context;
    private List<User> contacts;
    private AdapterListener listener;

    public ContactsAdapter(Context context, List<User> contacts, AdapterListener listener) {
        this.context = context;
        this.contacts = contacts;
        this.listener = listener;
    }

    @NonNull
    @Override
    public Item onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_contact, parent, false);
        return new Item(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Item holder, int position) {
        holder.contactName.setText(contacts.get(position).getDisplayName());
        AppDatabase db = AppDatabase.getDatabase(context);
        User user = contacts.get(position);
        if (db.requestDao().exists(user.getId()))
            holder.itemView.setEnabled(false);
        else
            holder.itemView.setEnabled(true);
        if (contacts.get(position).getAvatarUrl() != null && !contacts.get(position).getAvatar().isEmpty())
            Picasso.get().load(contacts.get(position).getAvatar()).error(R.drawable.placeholder).into(holder.profilePicture);
        else
            Picasso.get().load(R.drawable.placeholder).into(holder.profilePicture);
    }

    @Override
    public int getItemCount() {
        return contacts == null ? 0 : contacts.size();
    }

    class Item extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView contactName;
        ImageView profilePicture;

        Item(View view) {
            super(view);
            contactName = view.findViewById(R.id.contact_name);
            profilePicture = view.findViewById(R.id.profile_picture);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClick(v, this.getLayoutPosition());
        }
    }
}
