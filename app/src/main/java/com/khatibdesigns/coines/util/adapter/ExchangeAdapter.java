package com.khatibdesigns.coines.util.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.util.listener.AdapterListener;
import com.khatibdesigns.coines.util.model.Exchange;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ExchangeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<Exchange> exchangeList;
    private AdapterListener adapterListener;

    public ExchangeAdapter(Context context, ArrayList<Exchange> exchangeList, AdapterListener adapterListener) {
        this.context = context;
        this.exchangeList = exchangeList;
        this.adapterListener = adapterListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_exchange, parent, false);
        return new Item(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof Item) {
            Item viewHolder = (Item) holder;
            if (exchangeList.get(position) != null) {
                Exchange current = exchangeList.get(position);
                Picasso.get()
                        .load(R.drawable.lightpay)
                        .into(viewHolder.imgSelling);
                Picasso.get()
                        .load(R.drawable.ripple)
                        .into(viewHolder.imgReceiving);
                viewHolder.sellingAmount.setText(current.getSellingAmount());
                viewHolder.receivingAmount.setText(current.getReceivingAmount());
                if(current.getTime() != null) {
                    viewHolder.status.setText(current.getTime());
                    viewHolder.status.setCompoundDrawablesWithIntrinsicBounds(R.drawable.clock, 0, 0, 0);
                } else if(current.getStatus() != null) {
                    viewHolder.status.setText(current.getStatus());
                    if(current.getStatus().equals(context.getString(R.string.tap_details))) {
                        viewHolder.status.setCompoundDrawablesWithIntrinsicBounds(R.drawable.point_at, 0, 0, 0);
                    } else if(current.getStatus().equals(context.getString(R.string.failed))) {
                        viewHolder.status.setCompoundDrawablesWithIntrinsicBounds(R.drawable.failed, 0, 0, 0);
                    } else {
                        viewHolder.status.setCompoundDrawablesWithIntrinsicBounds(R.drawable.completed, 0, 0, 0);
                    }
                }
                viewHolder.status.setCompoundDrawablePadding(10);
            }
        }
    }

    @Override
    public int getItemCount() {
        return exchangeList == null ? 0 : exchangeList.size();
    }

    private class Item extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imgSelling, imgReceiving;
        TextView sellingAmount, receivingAmount, status;

        Item(View view) {
            super(view);
            imgSelling = view.findViewById(R.id.selling_image);
            imgReceiving = view.findViewById(R.id.receiving_image);
            sellingAmount = view.findViewById(R.id.selling_amount);
            receivingAmount = view.findViewById(R.id.receiving_amount);
            status = view.findViewById(R.id.status);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            adapterListener.onClick(v, this.getLayoutPosition());
        }
    }
}
