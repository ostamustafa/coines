package com.khatibdesigns.coines.util.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.khatibdesigns.coines.fragment.ActiveFragment;
import com.khatibdesigns.coines.fragment.FinishedFragment;
import com.khatibdesigns.coines.fragment.OffersFragment;

public class MainPagerAdapter extends FragmentPagerAdapter {

    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ActiveFragment.newInstance();
            case 1:
                return OffersFragment.newInstance();
            case 2:
                return FinishedFragment.newInstance();
            default:
                return ActiveFragment.newInstance();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}