package com.khatibdesigns.coines.util.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.util.database.AppDatabase;
import com.khatibdesigns.coines.util.listener.RequestCallback;
import com.khatibdesigns.coines.util.model.Dialog;
import com.khatibdesigns.coines.util.model.Request;
import com.khatibdesigns.coines.util.model.User;
import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.utils.DateFormatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.Item> {

    private Context context;
    private ArrayList<Request> requests;
    private RequestCallback callback;

    public RequestAdapter(Context context, ArrayList<Request> requests, RequestCallback callback) {
        this.context = context;
        this.requests = requests;
        this.callback = callback;
    }

    @NonNull
    @Override
    public Item onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.layout_request, parent, false);
        return new Item(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Item holder, int position) {
        AppDatabase db = AppDatabase.getDatabase(context);
        Request request = requests.get(position);
        User user = null;
        Dialog dialog = null;
        if (request.isFromUser())
            user = db.userDao().getUser(request.getReferenceId());
        else
            dialog = db.dialogDao().getDialog(request.getReferenceId());
        if (request.isSelfSent()) {
            holder.request.setVisibility(View.GONE);
            holder.pending.setVisibility(View.VISIBLE);
        } else {
            holder.pending.setVisibility(View.GONE);
            holder.request.setVisibility(View.VISIBLE);
        }
        if (user != null) {
            if (user.getAvatarUrl() != null && !user.getAvatarUrl().isEmpty())
                Picasso.get()
                        .load(user.getAvatar())
                        .error(R.drawable.placeholder)
                        .placeholder(R.drawable.placeholder)
                        .into(holder.requestAvatar);
            else
                Picasso.get()
                        .load(R.drawable.placeholder)
                        .into(holder.requestAvatar);
            holder.requestName.setText(user.getDisplayName());
        } else {
            if (dialog.getDialogPhoto() != null && !dialog.getDialogPhoto().isEmpty())
                Picasso.get()
                        .load(dialog.getDialogPhoto())
                        .error(R.drawable.placeholder)
                        .placeholder(R.drawable.placeholder)
                        .into(holder.requestAvatar);
            else
                Picasso.get()
                        .load(R.drawable.placeholder)
                        .into(holder.requestAvatar);
            holder.requestName.setText(dialog.getDialogName());
        }
        holder.requestDate.setText(format(request.getCreatedAt()));
        holder.previewMessage.setText(request.getPreviewMessage());
    }

    private String format(Date date) {
        if (DateFormatter.isToday(date)) {
            return DateFormatter.format(date, DateFormatter.Template.TIME);
        } else if (DateFormatter.isYesterday(date)) {
            return "Yesterday";
        } else if (sameWeek(date)) {
            return DateFormatter.format(date, "EEEE");
        } else if (DateFormatter.isCurrentYear(date)) {
            return DateFormatter.format(date, "dd MMM");
        } else
            return DateFormatter.format(date, "dd MMM yyyy");
    }

    private boolean sameWeek(Date date) {
        Calendar now = Calendar.getInstance();
        Calendar current = Calendar.getInstance();
        current.setTime(date);
        return now.get(Calendar.WEEK_OF_YEAR) == current.get(Calendar.WEEK_OF_YEAR);
    }

    @Override
    public int getItemCount() {
        return requests == null ? 0 : requests.size();
    }

    public void add(Request request) {
        requests.add(request);
        notifyDataSetChanged();
    }

    public void clear() {
        requests.clear();
        notifyDataSetChanged();
    }

    public void delete(Request request) {
        for (int i = 0; i < getItemCount(); i++) {
            Request current = requests.get(i);
            if (current.getId().equals(request.getId())) {
                requests.remove(i);
                notifyItemRemoved(i);
                break;
            }
        }
    }

    class Item extends RecyclerView.ViewHolder implements View.OnClickListener {

        CircleImageView requestAvatar;
        TextView requestName, requestDate, groupMembers, previewMessage, pending;
        ImageView accept, decline;
        LinearLayout request;

        Item(View itemView) {
            super(itemView);
            requestAvatar = itemView.findViewById(R.id.requestAvatar);
            requestName = itemView.findViewById(R.id.requestName);
            requestDate = itemView.findViewById(R.id.requestDate);
            groupMembers = itemView.findViewById(R.id.groupMembers);
            previewMessage = itemView.findViewById(R.id.previewMessage);
            accept = itemView.findViewById(R.id.accept);
            decline = itemView.findViewById(R.id.decline);
            pending = itemView.findViewById(R.id.pending);
            request = itemView.findViewById(R.id.request);
            accept.setOnClickListener(this);
            decline.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Request request = requests.get(getLayoutPosition());
            if (v.getId() == R.id.accept)
                callback.accept(request);
            else
                callback.decline(request);
        }
    }
}
