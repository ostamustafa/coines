package com.khatibdesigns.coines.util.database;

import android.content.Context;

import com.khatibdesigns.coines.util.database.dao.DialogDao;
import com.khatibdesigns.coines.util.database.dao.DialogUsersDao;
import com.khatibdesigns.coines.util.database.dao.MessageDao;
import com.khatibdesigns.coines.util.database.dao.RequestDao;
import com.khatibdesigns.coines.util.database.dao.UserDao;
import com.khatibdesigns.coines.util.model.Dialog;
import com.khatibdesigns.coines.util.model.DialogUsers;
import com.khatibdesigns.coines.util.model.Message;
import com.khatibdesigns.coines.util.model.Request;
import com.khatibdesigns.coines.util.model.User;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {
        User.class,
        Dialog.class,
        Message.class,
        DialogUsers.class,
        Request.class
},
        version = 7,
        exportSchema = false
)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {

    public abstract UserDao userDao();

    public abstract DialogDao dialogDao();

    public abstract MessageDao messageDao();

    public abstract DialogUsersDao dialogUsersDao();

    public abstract RequestDao requestDao();

    private static AppDatabase INSTANCE;

    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "coines_chat")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}