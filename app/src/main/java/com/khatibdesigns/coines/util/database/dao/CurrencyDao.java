package com.khatibdesigns.coines.util.database.dao;

import com.khatibdesigns.coines.util.model.Currency;

import java.util.ArrayList;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface CurrencyDao {

    @Insert
    void insert(Currency...currencies);

    @Delete
    void delete(Currency currency);

    @Update
    void update(Currency currency);

    @Query("SELECT * from currencies")
    ArrayList<Currency> getAll();

    @Query("SELECT * from currencies where ticker = :ticker")
    Currency get(String ticker);

}
