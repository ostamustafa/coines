package com.khatibdesigns.coines.util.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.khatibdesigns.coines.util.model.Dialog;
import com.khatibdesigns.coines.util.model.DialogUsers;

import java.util.List;

@Dao
public interface DialogDao {

    @Query("SELECT * FROM dialogs order by created_at")
    List<Dialog> getAll();

    @Query("SELECT * FROM dialogs WHERE id IN (:dialogIds)")
    List<Dialog> getDialogs(String[] dialogIds);

    @Query("SELECT * FROM dialogs WHERE id = :id")
    Dialog getDialog(String id);

    @Query("SELECT count(1) from dialogs where id = :id")
    boolean exists(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Dialog... dialogs);

    @Delete
    void delete(Dialog dialog);

    @Update
    void update(DialogUsers dialogUsers);
}
