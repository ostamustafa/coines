package com.khatibdesigns.coines.util.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.khatibdesigns.coines.util.model.Dialog;
import com.khatibdesigns.coines.util.model.DialogUsers;
import com.khatibdesigns.coines.util.model.User;

import java.util.List;

@Dao
public interface DialogUsersDao {

    @Query("SELECT * FROM users where id in (select user_id from dialog_users where dialog_id = :dialogId)")
    List<User> getUsers(String dialogId);

    @Query("SELECT * from dialogs where id in (select dialog_id from dialog_users where user_id = :id)")
    List<Dialog> getDialogs(String id);

    @Query("SELECT count(1) from dialog_users where user_id = :userId")
    boolean hasDialogs(String userId);

    @Query("Select users.* from users, dialogs where admin_id = users.id and dialogs.id = :id")
    User getAdmin(String id);

    @Insert
    void insert(DialogUsers... dialogUsers);

    @Delete
    void delete(DialogUsers dialogUsers);

    @Update
    void update(DialogUsers dialogUsers);
}
