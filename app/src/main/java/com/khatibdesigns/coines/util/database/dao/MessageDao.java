package com.khatibdesigns.coines.util.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.khatibdesigns.coines.util.model.Message;
import com.khatibdesigns.coines.util.model.User;

import java.util.List;

@Dao
public interface MessageDao {
    @Query("SELECT * from messages")
    List<Message> getAll();

    @Query("SELECT * from messages where id = :id")
    Message getMessages(String id);

    @Query("select * from messages where messages.dialog_id = :dialogId order by created_at desc")
    List<Message> getMessagesForDialog(String dialogId);

    @Query("SELECT * from users where id = (select user_id from messages where id = :id)")
    User getUser(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Message... messages);

    @Delete
    void delete(Message message);

    @Update
    void update(Message message);
}
