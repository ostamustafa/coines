package com.khatibdesigns.coines.util.database.dao;

import com.khatibdesigns.coines.util.model.Request;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface RequestDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Request... requests);

    @Query("delete from requests where self_sent = 0")
    void declineAll();

    @Delete
    void delete(Request... requests);

    @Query("delete from requests")
    void empty();

    @Query("Select * from requests")
    List<Request> getAll();

    @Query("select * from requests where id = :id")
    Request get(String id);

    @Query("Select count(1) from requests where reference_id = :id")
    boolean exists(String id);
}
