package com.khatibdesigns.coines.util.database.dao;

import com.khatibdesigns.coines.util.model.User;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface UserDao {
    @Query("SELECT * FROM users where main = 1")
    List<User> getMain();

    @Query("SELECT * FROM users WHERE id = :id")
    User getUser(String id);

    @Query("SELECT * from users where online = :status and id != :id and main = 1 order by display_name")
    List<User> getStatus(boolean status, String id);

    @Query("SELECT count(1) from users where id = :id")
    boolean exists(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(User... users);

    @Delete
    void delete(User user);

    @Update
    void update(User user);
}
