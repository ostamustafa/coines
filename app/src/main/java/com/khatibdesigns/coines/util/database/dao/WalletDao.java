package com.khatibdesigns.coines.util.database.dao;

import com.khatibdesigns.coines.util.model.Wallet;

import java.util.ArrayList;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

public interface WalletDao {

    @Insert
    void insert(Wallet...currencies);

    @Delete
    void delete(Wallet currency);

    @Update
    void update(Wallet currency);

    @Query("SELECT * from currencies")
    ArrayList<Wallet> getAll();

    @Query("SELECT * from wallets where id = :id")
    Wallet get(String id);
}