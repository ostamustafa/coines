package com.khatibdesigns.coines.util.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.util.database.AppDatabase;
import com.khatibdesigns.coines.util.model.Dialog;
import com.stfalcon.chatkit.dialogs.DialogsListAdapter;
import com.stfalcon.chatkit.utils.ShapeImageView;

public class GroupDialogViewHolder extends DialogsListAdapter.DialogViewHolder<Dialog> {

    private TextView dialogMembers, dialogDate;
    private ShapeImageView dialogLastMessageUserAvatar;

    public GroupDialogViewHolder(View itemView) {
        super(itemView);
        dialogMembers = itemView.findViewById(R.id.groupMembers);
        dialogDate = itemView.findViewById(R.id.dialogDate);
        dialogLastMessageUserAvatar = itemView.findViewById(R.id.dialogLastMessageUserAvatar);
    }

    @Override
    public void onBind(Dialog dialog) {
        super.onBind(dialog);
        dialogLastMessageUserAvatar.setVisibility(View.GONE);
        if (dialog.getUsers().size() > 1) {
            dialogMembers.setVisibility(View.VISIBLE);
            dialogDate.setVisibility(View.INVISIBLE);
            StringBuilder builder = new StringBuilder();
            builder.append(dialog.getUsers().size());
            builder.append(" members");
            dialogMembers.setText(builder);
        } else {
            dialogMembers.setVisibility(View.GONE);
            dialogDate.setVisibility(View.VISIBLE);
        }
    }


}
