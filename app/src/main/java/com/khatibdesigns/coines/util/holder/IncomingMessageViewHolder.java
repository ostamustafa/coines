package com.khatibdesigns.coines.util.holder;

import android.view.View;
import android.widget.TextView;

import com.khatibdesigns.coines.R;
import com.khatibdesigns.coines.util.model.Message;
import com.stfalcon.chatkit.messages.MessageHolders;

public class IncomingMessageViewHolder extends MessageHolders.IncomingTextMessageViewHolder<Message> {

    public IncomingMessageViewHolder(View itemView, Object payload) {
        super(itemView, payload);
    }

    @Override
    public void onBind(Message message) {
        super.onBind(message);
        String senderName = message.getUser().getName();
        ((TextView) itemView.findViewById(R.id.senderName)).setText(senderName);
    }
}
