package com.khatibdesigns.coines.util.listener;

import android.view.View;

public interface AdapterListener {
    void onClick(View v, int position);
}
