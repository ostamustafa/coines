package com.khatibdesigns.coines.util.listener;

import com.khatibdesigns.coines.util.model.Request;

public interface RequestCallback {
    void accept(Request request);
    void decline(Request request);
}
