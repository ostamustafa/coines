package com.khatibdesigns.coines.util.model;

import java.io.Serializable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "currencies")
public class Currency implements Serializable {
    @PrimaryKey
    @ColumnInfo(name = "ticker")
    @NonNull
    private String ticker;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "position")
    private String position;

    @ColumnInfo(name = "def_left")
    private Boolean defLeft;

    @ColumnInfo(name = "def_right")
    private Boolean defRight;

    @ColumnInfo(name = "network_fee")
    private Double networkFee;

    @ColumnInfo(name = "coines_fee")
    private Double coinesFee;

    @ColumnInfo(name = "min_for_exchange")
    private Double minForExchange;

    @ColumnInfo(name = "currency_logo")
    private String currencyLogo;

    public Currency(String name){
        this.name = name;
    }

    public Currency (String ticker, String name, String position, Boolean defLeft,
                     Boolean defRight, Double networkFee, Double coinesFee,
                     Double minForExchange, String currencyLogo) {
        this.ticker = ticker;
        this.name = name;
        this.position = position;
        this.defLeft = defLeft;
        this.defRight = defRight;
        this.networkFee = networkFee;
        this.coinesFee = coinesFee;
        this.minForExchange = minForExchange;
        this.currencyLogo = currencyLogo;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Boolean getDefLeft() {
        return defLeft;
    }

    public void setDefLeft(Boolean defLeft) {
        this.defLeft = defLeft;
    }

    public Boolean getDefRight() {
        return defRight;
    }

    public void setDefRight(Boolean defRight) {
        this.defRight = defRight;
    }

    public Double getNetworkFee() {
        return networkFee;
    }

    public void setNetworkFee(Double networkFee) {
        this.networkFee = networkFee;
    }

    public Double getCoinesFee() {
        return coinesFee;
    }

    public void setCoinesFee(Double coinesFee) {
        this.coinesFee = coinesFee;
    }

    public Double getMinForExchange() {
        return minForExchange;
    }

    public void setMinForExchange(Double minForExchange) {
        this.minForExchange = minForExchange;
    }

    public String getCurrencyLogo() {
        return currencyLogo;
    }

    public void setCurrencyLogo(String currencyLogo) {
        this.currencyLogo = currencyLogo;
    }
}
