package com.khatibdesigns.coines.util.model;

import com.stfalcon.chatkit.commons.models.IDialog;
import com.stfalcon.chatkit.commons.models.IMessage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "dialogs",
        foreignKeys = {
                @ForeignKey(
                        entity = User.class,
                        parentColumns = "id",
                        childColumns = "admin_id",
                        onDelete = ForeignKey.CASCADE
                )},
        indices = {
                @Index("admin_id")
        }
)
public class Dialog implements IDialog, Serializable {

    @PrimaryKey
    @ColumnInfo(index = true)
    @NonNull
    private String id;
    @ColumnInfo(name = "dialog_photo")
    private String dialogPhoto;
    @ColumnInfo(name = "dialog_name")
    private String dialogName;
    @ColumnInfo(name = "unread_count")
    private int unreadCount;
    @ColumnInfo(name = "admin_id")
    private String adminId;
    @Ignore
    private ArrayList<User> users;
    @Ignore
    private Message lastMessage;
    @ColumnInfo(name = "created_at")
    private Date createdAt;

    //private dialog
    @Ignore
    public Dialog(@NonNull String id, String dialogPhoto, String dialogName, ArrayList<User> users, Message lastMessage, int unreadCount, Date createdAt) {
        this(id, null, dialogPhoto, dialogName, unreadCount, createdAt);
        this.users = users;
        this.lastMessage = lastMessage;
    }

    //group dialog
    @Ignore
    public Dialog(@NonNull String id, String adminId, String dialogPhoto, String dialogName, ArrayList<User> users, Message lastMessage, int unreadCount, Date createdAt) {
        this(id, adminId, dialogPhoto, dialogName, unreadCount, createdAt);
        this.users = users;
        this.lastMessage = lastMessage;
    }

    public Dialog(@NonNull String id, String adminId, String dialogPhoto, String dialogName, int unreadCount, Date createdAt) {
        this.id = id;
        this.dialogPhoto = dialogPhoto;
        this.dialogName = dialogName;
        this.unreadCount = unreadCount;
        this.adminId = adminId;
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    @NonNull
    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getDialogPhoto() {
        return dialogPhoto;
    }

    @Override
    public String getDialogName() {
        return dialogName;
    }

    @Override
    public List<User> getUsers() {
        return users;
    }

    @Override
    public IMessage getLastMessage() {
        return lastMessage;
    }

    @Override
    public void setLastMessage(IMessage lastMessage) {
        this.lastMessage = (Message) lastMessage;
    }

    @Override
    public int getUnreadCount() {
        return unreadCount;
    }

    public void setDialogPhoto(String dialogPhoto) {
        this.dialogPhoto = dialogPhoto;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }
}
