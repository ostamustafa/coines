package com.khatibdesigns.coines.util.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "dialog_users",
        foreignKeys = {
                @ForeignKey(entity = User.class,
                        parentColumns = "id",
                        childColumns = "user_id"
                ),
                @ForeignKey(entity = Dialog.class,
                        parentColumns = "id",
                        childColumns = "dialog_id"
                )},
        indices = {
                @Index(value = "user_id"),
                @Index(value = "dialog_id")
        }
)
public class DialogUsers {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(index = true)
    private int id;
    @ColumnInfo(name = "dialog_id")
    private String dialogId;
    @ColumnInfo(name = "user_id")
    private String userId;

    public DialogUsers(String dialogId, String userId) {
        this.dialogId = dialogId;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public String getDialogId() {
        return dialogId;
    }

    public String getUserId() {
        return userId;
    }

    public void setId(int id) {
        this.id = id;
    }
}
