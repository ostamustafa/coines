package com.khatibdesigns.coines.util.model;

import java.io.Serializable;

public class Exchange implements Serializable {

    private String sellingImageUrl, receivingImageUrl, sellingAmount, receivingAmount, time, status;

    public Exchange(String sellingImageUrl, String receivingImageUrl, String sellingAmount, String receivingAmount, String time, String status) {
        this.sellingImageUrl = sellingImageUrl;
        this.receivingImageUrl = receivingImageUrl;
        this.sellingAmount = sellingAmount;
        this.receivingAmount = receivingAmount;
        this.time = time;
        this.status = status;
    }

    public String getSellingImageUrl() {
        return sellingImageUrl;
    }

    public String getReceivingImageUrl() {
        return receivingImageUrl;
    }

    public String getSellingAmount() {
        return sellingAmount;
    }

    public String getReceivingAmount() {
        return receivingAmount;
    }

    public String getTime() {
        return time;
    }

    public String getStatus() {
        return status;
    }
}
