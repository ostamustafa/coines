package com.khatibdesigns.coines.util.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.IUser;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "messages",
        foreignKeys = {
                @ForeignKey(entity = User.class,
                        parentColumns = "id",
                        childColumns = "user_id",
                        onDelete = ForeignKey.CASCADE
                ),
                @ForeignKey(entity = Dialog.class,
                        parentColumns = "id",
                        childColumns = "dialog_id"
                )
        },
        indices = {
                @Index(value = "user_id"),
                @Index(value = "dialog_id")
        })
public class Message implements IMessage, Serializable {

    @PrimaryKey
    @ColumnInfo(index = true)
    @NonNull
    private String id;
    private String text;
    @ColumnInfo(name = "user_id")
    private String userId;
    @ColumnInfo(name = "created_at")
    private Date createdAt;
    @ColumnInfo(name = "dialog_id")
    private String dialogId;
    @Ignore
    private User user;

    @Ignore
    public Message(String id, String text, User user, String dialogId, Date createdAt) {
        this(id, text, user.getId(), dialogId, createdAt);
        this.user = user;
    }

    public Message(String id, String text, String userId, String dialogId, Date createdAt) {
        this.id = id;
        this.text = text;
        this.userId = userId;
        this.createdAt = createdAt;
        this.dialogId = dialogId;
    }

    public String getDialogId() {
        return dialogId;
    }

    public void setDialogId(String dialogId) {
        this.dialogId = dialogId;
    }

    @Override
    public String getId() {
        return id;
    }

    @Ignore
    @Override
    public IUser getUser() {
        return user;
    }

    @Ignore
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String getText() {
        return text;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public Date getCreatedAt() {
        return createdAt;
    }
}
