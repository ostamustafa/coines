package com.khatibdesigns.coines.util.model;

import java.io.Serializable;

public class Notification implements Serializable {

    private String title, content, time;

    public Notification(String title, String content, String time) {
        this.title = title;
        this.content = content;
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getTime() {
        return time;
    }
}
