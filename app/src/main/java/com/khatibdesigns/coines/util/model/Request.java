package com.khatibdesigns.coines.util.model;

import java.util.Date;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "requests")
public class Request {

    @PrimaryKey
    @ColumnInfo(index = true)
    @NonNull
    private String id;
    @ColumnInfo(name = "preview_message")
    private String previewMessage;
    @ColumnInfo(name = "reference_id")
    private String referenceId;
    @ColumnInfo(name = "self_sent")
    private boolean selfSent;
    @ColumnInfo(name = "created_at")
    private Date createdAt;
    @ColumnInfo(name = "from_user")
    private boolean fromUser;

    public Request(String id, String previewMessage, String referenceId, Date createdAt, boolean selfSent, boolean fromUser) {
        this.id = id;
        this.previewMessage = previewMessage;
        this.referenceId = referenceId;
        this.createdAt = createdAt;
        this.selfSent = selfSent;
        this.fromUser = fromUser;
    }

    public boolean isFromUser() {
        return fromUser;
    }

    public void setFromUser(boolean fromUser) {
        this.fromUser = fromUser;
    }

    public boolean isSelfSent() {
        return selfSent;
    }

    public void setSelfSent(boolean selfSent) {
        this.selfSent = selfSent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPreviewMessage() {
        return previewMessage;
    }

    public void setPreviewMessage(String previewMessage) {
        this.previewMessage = previewMessage;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }
}
