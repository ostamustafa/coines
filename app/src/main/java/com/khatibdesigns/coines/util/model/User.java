package com.khatibdesigns.coines.util.model;

import com.stfalcon.chatkit.commons.models.IUser;

import java.io.Serializable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "users")
public class User implements IUser, Serializable {

    @PrimaryKey
    @ColumnInfo(index = true)
    @NonNull
    private String id;
    @ColumnInfo(name = "display_name")
    private String displayName;
    private String username;
    @ColumnInfo(name = "avatar_url")
    private String avatarUrl;
    @ColumnInfo(name = "online")
    private boolean online;
    @ColumnInfo(name = "phone_number")
    private String phoneNumber;
    @ColumnInfo(name = "push_token")
    private String pushToken;
    @ColumnInfo(name = "main")
    private boolean main;

    public User(String id, String displayName, String username, String avatarUrl, String phoneNumber, Boolean online, String pushToken, boolean main) {
        this.id = id;
        this.displayName = displayName;
        this.username = username;
        this.avatarUrl = avatarUrl;
        this.phoneNumber = phoneNumber;
        this.online = online;
        this.pushToken = pushToken;
        this.main = main;
    }

    public boolean isMain() {
        return main;
    }

    public void setMain(boolean main) {
        this.main = main;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String getAvatar() {
        return avatarUrl;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getUsername() {
        return username;
    }

    public boolean isOnline() {
        return online;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
