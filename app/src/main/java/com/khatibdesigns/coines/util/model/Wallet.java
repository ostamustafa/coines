package com.khatibdesigns.coines.util.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;


@Entity(tableName = "wallets",
        foreignKeys = {
                @ForeignKey(entity = Currency.class,
                        parentColumns = "ticker",
                        childColumns = "currency_id"
                )
        },
        indices = {
                @Index(value = "currency_id")
        })
public class Wallet {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private String name;

    private String address;

    @ColumnInfo(name = "currency_id")
    private Boolean currencyId;


    public Wallet(int id, String name, String address, Boolean currencyId) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.currencyId = currencyId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Boolean currencyId) {
        this.currencyId = currencyId;
    }
}
