package com.khatibdesigns.coines.util.sprint;

public class APIEndPoint {
    private static final String URL = "https://api.cnes.project.lhost.bg/";

    public static final String VALIDATE_AMOUNT = URL + "validateamount";
    public static final String VALIDATE_ADDRESS = URL + "validateaddress";
    public static final String CURRENCIES = URL + "currencies";
}
