package com.khatibdesigns.coines.util.sprint;

import android.os.AsyncTask;
import android.util.SparseArray;

import com.khatibdesigns.coines.util.sprint.header.Header;
import com.khatibdesigns.coines.util.sprint.multipart.MultiPartFile;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class ConnectionTask extends AsyncTask<Void, Integer, SparseArray<String>> {

    private String url;
    private HashSet<Header> headers;
    private HashMap<String, String> arguments;
    private ArrayList<MultiPartFile> files;
    private Method method;
    private SprintListener listener;

    ConnectionTask(String url, HashMap<String, String> arguments, ArrayList<MultiPartFile> files, HashSet<Header> headers, Method method, SprintListener listener) {
        this.url = url;
        this.listener = listener;
        this.arguments = arguments;
        this.method = method;
        this.headers = headers;
        this.files = files;
    }

    @Override
    protected SparseArray<String> doInBackground(Void... Void) {
        if (url == null || listener == null)
            return null;
        SparseArray<String> result;
        try {
            SprintUtil util = new SprintUtil(url, headers, arguments, files, method);
            result = util.request();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(SparseArray<String> result) {
        if (result.size() == 0)
            listener.onFailure(null, HttpURLConnection.HTTP_NO_CONTENT);
        else if (result.size() == 1) {
            int key = result.keyAt(0);
            try {
                if (key == HttpURLConnection.HTTP_OK)
                    listener.onSuccess(new JSONObject(result.get(key)));
                else
                    listener.onFailure(new JSONObject(result.get(key)), key);
            } catch (JSONException e) {
                e.printStackTrace();
                listener.onFailure(null, 500);
            }
        } else {
            listener.onFailure(null, HttpURLConnection.HTTP_NOT_FOUND);
        }
    }
}
