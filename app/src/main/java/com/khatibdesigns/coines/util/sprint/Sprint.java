package com.khatibdesigns.coines.util.sprint;

import com.khatibdesigns.coines.util.sprint.header.Accept;
import com.khatibdesigns.coines.util.sprint.header.ContentType;
import com.khatibdesigns.coines.util.sprint.header.Header;
import com.khatibdesigns.coines.util.sprint.multipart.MultiPartFile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

public class Sprint {

    private static volatile Sprint sprint;
    private SprintResult result;
    private Method method;
    private HashSet<Header> headers;

    private Sprint() {
        if (sprint != null)
            throw new RuntimeException("Use instance() method to get the single instance of this class.");
    }

    public static Sprint instance() {
        return instance(Method.GET);
    }

    public static Sprint instance(Method method) {
        if (sprint == null) {
            synchronized (Sprint.class) {
                if (sprint == null)
                    sprint = new Sprint();
            }
        }
        sprint.method = method;
        return sprint.headers(ContentType.URL_ENCODED, Accept.JSON);
    }

    public Sprint headers(Header... headers) {
        this.headers = new HashSet<>();
        Collections.addAll(this.headers, headers);
        return this;
    }

    public SprintResult request(String url) {
        return request(url, null, null);
    }

    public SprintResult request(String url, HashMap<String, String> arguments) {
        return request(url, arguments, null);
    }

    public SprintResult request(String url, ArrayList<MultiPartFile> files) {
        return request(url, null, files);
    }

    public SprintResult request(String url, HashMap<String, String> arguments, ArrayList<MultiPartFile> files) {
        result = new SprintResult(url, arguments, files, headers, method);
        return result;
    }

    public void cancel() {
        if (result != null)
            result.cancel();
    }
}
