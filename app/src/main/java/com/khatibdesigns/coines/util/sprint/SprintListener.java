package com.khatibdesigns.coines.util.sprint;

import org.json.JSONObject;

public interface SprintListener {
    void onSuccess(JSONObject data);

    void onFailure(JSONObject data, int statusCode);
}
