package com.khatibdesigns.coines.util.sprint;

import android.os.AsyncTask;

import com.khatibdesigns.coines.util.sprint.header.Header;
import com.khatibdesigns.coines.util.sprint.multipart.MultiPartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class SprintResult {

    private String url;
    private Method method;
    private ArrayList<MultiPartFile> files;
    private HashMap<String, String> arguments;
    private HashSet<Header> headers;
    private ConnectionTask task;

    SprintResult(String url, HashMap<String, String> arguments, ArrayList<MultiPartFile> files, HashSet<Header> headers, Method method) {
        this.url = url;
        this.method = method;
        this.arguments = arguments;
        this.files = files;
        this.headers = headers;
    }

    public void execute(SprintListener listener) {
        task = new ConnectionTask(url, arguments, files, headers, method, listener);
        task.execute();
    }

    void cancel() {
        if (task.getStatus() != AsyncTask.Status.FINISHED)
            task.cancel(true);
    }

}