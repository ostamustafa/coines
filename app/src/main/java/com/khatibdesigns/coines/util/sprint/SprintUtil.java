package com.khatibdesigns.coines.util.sprint;

import android.util.SparseArray;

import com.khatibdesigns.coines.util.sprint.header.ContentType;
import com.khatibdesigns.coines.util.sprint.header.Header;
import com.khatibdesigns.coines.util.sprint.multipart.MultiPartFile;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

class SprintUtil {

    private static final String CRLF = "\r\n";
    private static final String TWO_HYPHENS = "--";
    private static final String BOUNDARY = "----SprintRequest";
    private static final String UTF8 = "UTF-8";

    private HttpURLConnection connection;
    private String url;
    private HashSet<Header> headers;
    private HashMap<String, String> arguments;
    private ArrayList<MultiPartFile> files;
    private Method method;

    SprintUtil(String url, HashSet<Header> headers, HashMap<String, String> arguments, ArrayList<MultiPartFile> files, Method method) throws Exception {
        this.url = url;
        this.headers = headers;
        this.arguments = arguments;
        this.files = files;
        this.method = method;
        init();
    }

    private void init() throws Exception {
        URL url1 = new URL(url);
        connection = (HttpURLConnection) url1.openConnection();
        connection.setRequestMethod(method.getMethod());
        if (method != Method.GET)
            connection.setDoOutput(true);
        connection.setConnectTimeout(15000);
        connection.setUseCaches(false);
        setHeaders();
    }

    private void setHeaders() {
        connection.setRequestProperty("Connection", "Keep-Alive");
        for (Header header : headers) {
            if (!header.value().isEmpty())
                connection.setRequestProperty(header.key(), header.value());
        }
    }

    SparseArray<String> request() {
        ContentType contentType = getContentType();
        switch (contentType) {
            case FORM_DATA:
                return formData();
            case JSON:
                return json();
            case URL_ENCODED:
                return urlEncoded();
            default:
                return raw();
        }
    }

    private SparseArray<String> formData() {
        setupFormData();
        SparseArray<String> result = new SparseArray<>();
        try {
            DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
            if (arguments != null) {
                outputStream.writeBytes(getFormDataString());
                outputStream.flush();
            }
            if (files != null)
                for (MultiPartFile file : files)
                    file.writeDate(outputStream);
            outputStream.writeBytes(TWO_HYPHENS);
            outputStream.writeBytes(BOUNDARY);
            outputStream.writeBytes(TWO_HYPHENS);
            outputStream.writeBytes(CRLF);
            outputStream.flush();
            outputStream.close();
            checkStatus(result);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                connection.disconnect();
        }
        return result;
    }

    private SparseArray<String> json() {

        SparseArray<String> result = new SparseArray<>();
        try {
            if (arguments != null) {
                JSONObject jsonArgs = new JSONObject(arguments);
                connection.setDoOutput(true);
                DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
                outputStream.writeBytes(jsonArgs.toString());
                outputStream.flush();
                outputStream.close();
            }
            checkStatus(result);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                connection.disconnect();
        }
        return result;
    }

    private SparseArray<String> raw() {
        return null;
    }

    private SparseArray<String> urlEncoded() {
        SparseArray<String> result = new SparseArray<>();
        try {
            if (arguments != null) {
                connection.setFixedLengthStreamingMode(getUrlEncodedString().getBytes().length);
//                connection.setRequestProperty("Content-Length", Integer.toString(getUrlEncodedString().getBytes().length));
                //Send request
                connection.setDoOutput(true);
                DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
                outputStream.writeBytes(getUrlEncodedString());
                outputStream.flush();
                outputStream.close();
            }

            checkStatus(result);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                connection.disconnect();
        }
        return result;
    }

    private void checkStatus(SparseArray<String> result) throws IOException {
        int status = connection.getResponseCode();
        InputStream inputStream;
        //Get Response
        if (status == HttpURLConnection.HTTP_OK)
            inputStream = connection.getInputStream();
        else
            inputStream = connection.getErrorStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        StringBuilder response = new StringBuilder();
        while ((line = bufferedReader.readLine()) != null) {
            response.append(line);
            response.append('\r');
            result.put(status, response.toString());
        }
        bufferedReader.close();
        inputStream.close();
    }

    private String getUrlEncodedString() throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : arguments.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(entry.getKey(), UTF8));
            result.append("=");
            switch (entry.getValue()) {
                case "true":
                    result.append(URLEncoder.encode(String.valueOf(true), UTF8));
                    break;
                case "false":
                    result.append(URLEncoder.encode(String.valueOf(false), UTF8));
                    break;
                default:
                    result.append(URLEncoder.encode(entry.getValue(), UTF8));
                    break;
            }
        }

        return result.toString();

    }

    private void setupFormData() {
        connection.setChunkedStreamingMode(0);
        ContentType contentType = getContentType();
        connection.setRequestProperty(contentType.key(), contentType.value() + "; boundary=" + BOUNDARY);
    }

    private String getFormDataString() throws IOException {
        StringBuilder result = new StringBuilder();
        if (arguments != null) {
            for (Map.Entry<String, String> entry : arguments.entrySet()) {
                result.append(TWO_HYPHENS).append(BOUNDARY)
                        .append(CRLF)
                        .append("Content-Disposition: form-data; ")
                        .append("name=")
                        .append('"')
                        .append(URLEncoder.encode(entry.getKey(), UTF8))
                        .append('"')
                        .append(CRLF)
                        .append(CRLF);
                switch (entry.getValue()) {
                    case "true":
                        result.append(URLEncoder.encode(String.valueOf(true), UTF8));
                        break;
                    case "false":
                        result.append(URLEncoder.encode(String.valueOf(false), UTF8));
                        break;
                    default:
                        result.append(URLEncoder.encode(entry.getValue(), UTF8));
                        break;
                }
                result.append(CRLF);
            }
        }
        return result.toString();
    }

    private ContentType getContentType() {
        ContentType contentType = ContentType.RAW;
        for (Header header : headers)
            if (header instanceof ContentType)
                contentType = (ContentType) header;
        return contentType;
    }
}
