package com.khatibdesigns.coines.util.sprint.header;

public enum ContentType implements Header {
    JSON("application/json"),
    FORM_DATA("multipart/form-data"),
    URL_ENCODED("application/x-www-form-urlencoded"),
    RAW("");

    private String value;

    ContentType(String value) {
        this.value = value;
    }

    @Override
    public String key() {
        return "Content-Type";
    }

    @Override
    public String value() {
        return value;
    }

    @Override
    public void value(String value) {
        this.value = value;
    }
}
