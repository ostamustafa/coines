package com.khatibdesigns.coines.util.sprint.header;

public interface Header {
    String key();
    String value();
    void value(String value);
}
