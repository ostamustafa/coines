package com.khatibdesigns.coines.util.sprint.multipart;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;

public class MultiPartFile {

    private static final String CRLF = "\r\n";
    private static final String TWO_HYPHENS = "--";
    private static final String BOUNDARY = "----SprintRequest";

    private File file;
    private String name;

    public MultiPartFile(String name, File file) {
        this.file = file;
        this.name = name;
    }

    public void writeDate(DataOutputStream stream) throws IOException {
        String builder = TWO_HYPHENS + BOUNDARY +
                CRLF +
                "Content-Disposition: form-data; name=\"" + name + "\"; filename=\"" + file.getName() + '"' +
                CRLF +
                "Content-Type: image/" + getExtension().toLowerCase() +
                CRLF + CRLF;
        stream.writeBytes(builder);
        stream.write(getBytes());
        stream.writeBytes(CRLF);
        stream.flush();
    }

    private byte[] getBytes() {
        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(getFormat(), 80, stream);
        return stream.toByteArray();
    }

    private String getExtension() {
        int length = file.getName().length();
        int dotIndex = file.getName().indexOf('.');
        return file.getName().substring(dotIndex + 1, length).toUpperCase();
    }

    private Bitmap.CompressFormat getFormat() {
        return Bitmap.CompressFormat.valueOf(getExtension());
    }
}
